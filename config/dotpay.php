<?php
return [

    'api' => [
        'api_version' => 'dev',
        'username' => env('DOTPAY_USERNAME'),
        'password' => env('DOTPAY_PASSWORD'),
        'shop_id' => env('DOTPAY_SHOP_ID'),
        'pin' => env('DOTPAY_PIN'),
        'base_url' => env('DOTPAY_BASE_URL')
    ],
    'options' => [
        'url' => 'abcleague.webup-dev.pl',
        'curl' => 'callback_url',
        'recipient' => [
            'company' => 'YourCompany',
            'address' => [
                'street' => '',
                'building_number' => '',
                'postcode' => '',
                'city' => "Warszawa"
            ]
        ],
    ]
];
