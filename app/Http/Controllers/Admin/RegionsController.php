<?php

namespace App\Http\Controllers\Admin;

use App\AbcLeague\Interfaces\AdminRepositoryInterface;
use App\Http\Controllers\Controller;
use App\Region;
use Illuminate\Http\Request;

class RegionsController extends Controller
{
    public function __construct(AdminRepositoryInterface $adminRepository) /* Lecture 13 FrontendRepositoryInterface */
    {

        $this->aR = $adminRepository;

    }

    public function regions()
    {
        $regions = $this->aR->getAllRegions();
        return view('Admin.regions',  ['regions' => $regions]);
    }

    public function regions_add()
    {
        return view('Admin.add_region');
    }

    public function regions_edit($id)
    {
        $regions = $this->aR->getRegion($id);
        return view('Admin.edit_region',  ['regions' => $regions]);
    }

    public function store_region(Request $request)
    {
        request()->validate([

            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);
        $imageName = time().'.'.request()->image->getClientOriginalExtension();

        request()->image->move(public_path('images/Regions'), $imageName);


        $region = new Region;
        $region->name = $request->name;
        $region->avalible = 0;
        $region->full_name = $request->full_name;
        $region->image = $imageName;
        $region->save();
        return redirect()->route('admin_regions_add');
    }
    public function edit_region_save($id, Request $request)
    {
        request()->validate([

            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);
        $imageName = time().'.'.request()->image->getClientOriginalExtension();

        request()->image->move(public_path('images/Regions'), $imageName);

        $region = Region::findOrFail($id);
        $region->name = $request->name;
        $region->full_name = $request->full_name;
        $region->image = $imageName;
        $region->update();

        return redirect()->route('admin_regions_add');
    }

    public function delete_region($id)
    {
        $region = Region::findOrFail($id)->delete();

        return redirect()->route('admin_regions');

    }

}
