<?php

namespace App\Http\Controllers\Admin;

use App\AbcLeague\Interfaces\AdminRepositoryInterface;
use App\Http\Controllers\Controller;
use App\Review;
use Illuminate\Http\Request;

class ReviewsController extends Controller
{
    public function __construct(AdminRepositoryInterface $adminRepository) /* Lecture 13 FrontendRepositoryInterface */
    {

        $this->aR = $adminRepository;

    }

    public function reviews()
    {
        $reviews = $this->aR->getAllReviews();
        return view('Admin.reviews',  ['reviews' => $reviews]);
    }

    public function reviews_add()
    {

        return view('Admin.add_review');
    }

    public function reviews_edit($id)
    {
        $reviews = $this->aR->getReview($id);
        return view('Admin.edit_review', ['reviews' => $reviews]);
    }

    public function delete_review($id)
    {
        $review = Review::findOrFail($id)->delete();

        return redirect()->route('admin_reviews');

    }

    public function edit_review_save($id, Request $request)
    {
        $review = Review::findOrFail($id);

        $review->tekst = $request->tekst;
        $review->author = $request->author;
        $review->stars = $request->stars;
        $review->save();

        return redirect()->route('admin_reviews');
    }

    public function store_review(Request $request)
    {
        $review = new Review;

        $review->tekst = $request->tekst;
        $review->author = $request->author;
        $review->save();
        return redirect()->route('admin_reviews');
    }
}
