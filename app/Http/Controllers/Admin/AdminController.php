<?php

namespace App\Http\Controllers\Admin;

use App\Account;
use App\Http\Controllers\Controller;
use App\AbcLeague\Interfaces\AdminRepositoryInterface;
use App\Checkout;
use App\Order;
use App\Tos;
use Illuminate\Http\Request;
use App\Exports\OrdersExport;
use App\Exports\OrdersCountryExport;
use App\Exports\AllOrdersExport;
use Maatwebsite\Excel\Facades\Excel;


class AdminController extends Controller
{
    public function __construct(AdminRepositoryInterface $adminRepository) /* Lecture 13 FrontendRepositoryInterface */
    {
        $this->aR = $adminRepository;
    }

    public function export()
    {
        return Excel::download(new OrdersExport, 'Obrot_hajsiwa.xlsx');
    }

    public function export_country($id)
    {
        return Excel::download(new OrdersCountryExport($id), 'Hajsy_z_kazdego_kraju_pogrupowane.xlsx');
    }

    public function export_spis()
    {
        return Excel::download(new AllOrdersExport, 'Wszystkie zamówienia.xlsx');

    }

    public function invoice($id)
    {
        $invoice = Order::findOrFail($id);
        $kod = Order::findOrFail($id);
        $someArray = json_decode($kod->code, true);
//        return dd($someArray[0]["code"]);
        return view('Admin.invoice', ['invoice' => $invoice, 'code' => $someArray[0]["code"]]);
    }

    public function index()
    {
        $month = Order::selectRaw(' month(created_at) as month, year(created_at) as year')->groupBy('month')->get();

//        $country = Order::select('countrycode')->pluck('countrycode');
//        $order = Order::select('countrycode', 'price', 'fee')->where('countrycode', $country)->sum('price');
//        foreach ($country as $country){
//            $order = Order::select('countrycode', 'price', 'fee')->where('countrycode', $country)->sum('price');
//        }
//        return dd($order);
//        return view('export.excel', ['invoice' => $order]);

        $country = Order::all();

        $orders_count = $this->aR->getOrdersCount();
        $orders = $this->aR->getOrders();
        $count_acc = $this->aR->getAccountsAllCount();
        $euro = Order::where('currency', 'EUR')->where('status', 'Zaplacono')->sum('price');
        $dollar = Order::where('currency', 'USD')->where('status', 'Zaplacono')->sum('price');

        return view('Admin.index', ['orders_count' => $orders_count, 'orders' => $orders, 'count_acc' => $count_acc, 'euro' => $euro, 'dollar' => $dollar, 'country' => $country, 'month' => $month]);
    }

    public function showw()
    {
        $month = Order::selectRaw(' month(created_at) as month, year(created_at) as year')->groupBy('month')->get();

//        $country = Order::select('countrycode')->pluck('countrycode');
//        $order = Order::select('countrycode', 'price', 'fee')->where('countrycode', $country)->sum('price');
//        foreach ($country as $country){
//            $order = Order::select('countrycode', 'price', 'fee')->where('countrycode', $country)->sum('price');
//        }
//        return dd($order);
//        return view('export.excel', ['invoice' => $order]);

        $country = Order::all();

        $orders_count = $this->aR->getOrdersCount();
        $orders = Order::where('status','Zaplacono')->orderBy('created_at', 'desc')->get();
        $count_acc = $this->aR->getAccountsAllCount();
        $euro = Order::where('currency', 'EUR')->where('status', 'Zaplacono')->sum('price');
        $dollar = Order::where('currency', 'USD')->where('status', 'Zaplacono')->sum('price');

        return view('Admin.index', ['orders_count' => $orders_count, 'orders' => $orders, 'count_acc' => $count_acc, 'euro' => $euro, 'dollar' => $dollar, 'country' => $country, 'month' => $month]);
    }
    public function edit_tos()
    {
        $tos = Tos::first();
        return view('Admin.tos', ['tos' => $tos]);
    }

    public function update_tos(Request $request)
    {
        $tos = Tos::first();

        $tos->text = $request->description;
        $tos->update();
        return redirect()->route('tos_edit');
    }

    public function customize()
    {
        $check = Checkout::all();
        return view('Admin.customize_list', ['check' => $check]);
    }
    public function customize_add()
    {
        $accounts = $this->aR->getAllAccounts();
        return view('Admin.customize', ['accounts' => $accounts]);
    }

    public function customize_add_store(Request $request)
    {
        Checkout::insert([
            'name' => $request->name,
            'price' => $request->price,
            'description' => $request->description,
            'currency' => $request->currency,
            'account_id' => $request->account_id,
            'src' => $request->src,

        ]);
        return redirect()->route('admin_customize');
    }

    public function coupon()
    {
        return view('Admin.coupon');
    }

    public function delete($id)
    {
        Order::findOrFail($id)->delete();
        return redirect()->back();
    }



}
