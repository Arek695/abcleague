<?php

namespace App\Http\Controllers\Admin;

use App\AbcLeague\Interfaces\AdminRepositoryInterface;
use App\Account;
use App\Code;
use App\Http\Controllers\Controller;
use App\Region;
use Illuminate\Http\Request;

class AccountsController extends Controller
{
    public function __construct(AdminRepositoryInterface $adminRepository) /* Lecture 13 FrontendRepositoryInterface */
    {
        $this->aR = $adminRepository;
    }

    public function accounts($id)
    {
        $req_url = 'https://api.exchangerate-api.com/v4/latest/EUR';
        $response_json = file_get_contents($req_url);
        if(false !== $response_json) {

            try {

                $response_object = json_decode($response_json);

                $base_price = 1; // Your price in USD
                $USD_price = round(($base_price * $response_object->rates->USD), 2);

            }
            catch(Exception $e) {
                // Handle JSON parse error...
            }

        }
        $waluta = $USD_price;
        $account = $this->aR->getAccount($id);
        $count = $this->aR->getAccountsCount($id);
        $codes = Code::where('account_id', $id)->get();

        return view('Admin.accounts',  ['account' => $account, 'count'=>$count, 'code' => $codes, 'waluta' => $waluta]);
    }
    public function accounts_list()
    {
        $regions = Region::all();
        $account = $this->aR->getAllAccounts();

        return view('Admin.accounts_list',  ['account' => $account, 'regions' => $regions]);
    }

    public function accounts_add()
    {
        $regions = $this->aR->getAllRegions();

        return view('Admin.add_accounts',  ['regions' => $regions]);
    }

    public function edit_accounts_store($id, Request $request)
    {

        $ids = explode("\n", str_replace("\r", "", $request->konta));

        foreach($ids as $idss)
        {
            Code::where([
                'account_id' => $id,
            ])->delete();
        }

        foreach($ids as $idss)
        {
            Code::insert([
                'account_id' => $id,
                'code' => $idss,
            ]);
        }

        Code::where('code' , '')->delete();



        $acc = Account::findOrFail($id);
        $acc->name = $request->name;
        $acc->description = $request->description;
        $acc->price = $request->price;
        $acc->price_eur = $request->price_eur;
        $acc->src = $request->src;
        $acc->slug = $request->slug;
        $acc->order = $request->order;
        $acc->update();

        $region2 = Region::findOrFail($acc->region_id);
        $region2->avalible = 1;
        $region2->style = '';
        $region2->update();

        return redirect()->back();
    }

    public function accounts_add_store(Request $request)
    {


        Account::insert([
            'region_id' => $request->region_id,
            'name' => $request->name,
            'price' => $request->price_usd,
            'price_eur' => $request->price_eur,
            'description' => $request->description,
            'src' => $request->src,
            'currency' => 'USD'
        ]);
        $region = Region::findOrFail($request->region_id);
        $region->avalible = 1;
        $region->style = 'brak';
        $region->update();
        return redirect()->route('admin_accounts_list');
    }

    public function delete_account($id)
    {
        $account = Account::findOrFail($id)->delete();
        return redirect()->back();
    }
}
