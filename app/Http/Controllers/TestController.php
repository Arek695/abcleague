<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use CoinbaseCommerce\ApiClient;
use CoinbaseCommerce\Resources\Checkout;

class TestController extends Controller
{
    public function pyk()
    {
        return view('Front.test');
    }

    public function index()
    {
        ApiClient::init('8dda50fc-2c39-4470-b098-001499e8c2e1');
        $checkoutData = [
            'name' => 'The Sovereign Individual',
            'description' => 'Mastering the Transition to the Information Age',
            'pricing_type' => 'fixed_price',
            'pricing' => 'bitcoin',
            'local_price' => [
                'amount' => '0.50',
                'currency' => 'BTC'
            ],
            'requested_info' => ['name', 'email']
        ];
        $newCheckoutObj = Checkout::create($checkoutData);
//        return dd($newCheckoutObj->id);
        return view('test', ['pyk' => $newCheckoutObj->id]);
    }
}
