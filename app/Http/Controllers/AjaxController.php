<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Coupon;

class AjaxController extends Controller
{
    public function index(Request $request)
    {
        $cc = Coupon::where('first_name', $request->coupon_test)->first();

        if($cc == NULL){
            return response()->json(['failed'=>'Couppon is invalid']);
        }else{
            return response()->json(['success'=>'Couppon applied', 'msg'=>'true', 'discount'=>$cc->last_name]);
        }
    }
}
