<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Support\Facades\Config;
// Import the class namespaces first, before using it directly
use Srmklive\PayPal\Services\ExpressCheckout;
use Srmklive\PayPal\Services\AdaptivePayments;
use App\AbcLeague\Interfaces\AdminRepositoryInterface;
use App\Account;
use App\Code;
use Evilnet\Dotpay\DotpayManager;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Stripe\Charge;
use Stripe\Stripe;
use Illuminate\Support\Facades\DB;
use CoinbaseCommerce\ApiClient;
use CoinbaseCommerce\Resources\Checkout;


class PayPalController extends Controller
{
    public $provider;
    public $PLN;
    public $description;
    static public $test;


    public function __construct(AdminRepositoryInterface $adminRepository) /* Lecture 13 FrontendRepositoryInterface */
    {
        $this->provider = new ExpressCheckout;
        $this->aR = $adminRepository;
    }
    public function create_order(Request $request)
    {

//        $warranty_check = $request->warranty_check;
        $currency = $request->currency;
        $price = $request->price;
        $src = $request->src;
        $coupon = $request->kupon;
        $order_id = $request->order_id;
        $email = $request->email;
        $quantity = $request->quantity;
        $feeE = 0.03*$price;
        $fee = round($feeE, 2);
        $price_totalL = $fee + $price;
        $price_total = round($price_totalL, 2);
        $dev = $request->dev;
        $this->aR->prepare_order($request->description, $email, 'PL', 'Płatność rozpoczęta', '', 'No', $request->order_id, $price, $fee, $currency, $quantity, $src, $dev);


        if($request->type == "stripe")
        {
            return $this->stripeIndex($order_id);
        }
        if($request->type == "paypal")
        {
            return $this->payment($order_id);

        }

    }

    public function stripeIndex($order_id)
    {
        $order = Order::where('order_id', $order_id)->first();
        ApiClient::init('8dda50fc-2c39-4470-b098-001499e8c2e1');
        $checkoutData = [
            'name' => 'ABC League',
            'description' =>  $order->description,
            'pricing_type' => 'fixed_price',
            'local_price' => [
                'amount' => $order->price,
                'currency' => $order->currency
            ],
            'requested_info' => ['name', 'email']
        ];
        $newCheckoutObj = Checkout::create($checkoutData);


        $name = $order->description;
        $warranty_check = $order->warranty_check;
        $currency = $order->currency;
        $price = $order->price;
        $src = $order->src;

        $coupon = '123';
        $email = $order->email;
        $quantity = $order->quantity;
        $feeE = 0.03*$price;
        $fee = round($feeE, 2);
        $price_totalL = $fee + $price;
        $price_total = round($price_totalL, 2);

        return view('Front.Checkout4', [
                'name' => $name, 'warranty_check' => $warranty_check, 'currency' => $currency, 'price' => $price, 'src'=> $src, 'coupon' => $coupon,
                'order_id' => $order_id, 'email' => $email, 'quantity' => $quantity, 'pyk' => $newCheckoutObj->id]
        );
    }
    public function stripePost(Request $request)
    {
        $order_id = $request->order_id;
        $email = $request->email;
        $name = $request->name;
        $quantity = $request->quantity;
        $lastsur = $request->lastsur;
        Stripe::setApiKey('sk_live_TqQ81eH9fDYIJdCq9y3BLEKu00NSWw5FdP');
        try {
            Charge::create(array(
                "amount" => $request->price * 100,
                "currency" => $request->currency,
                "source" => $request->input('stripeToken'), // obtained with Stripe.js
                "description" => $request->name." by ".$request->email
            ));
            Session::flash('success-message', 'Payment done successfully !');



            $acc = Account::where('name', $request->name )->get();
            $code = Code::where('account_id', $acc[0]->id)->get()->take($quantity);

            $order = Order::where('order_id', $order_id)->first();

            $order->status = 'Zaplacono';
            $order->payment = 'Stripe';
            $order->name = $lastsur;
            $order->code = $code;
            $order->update();

            Mail::send( 'mail.index',['code' => $code, 'email' => $email, 'name' => $name, 'order_id' => $order_id] ,function($message) use ($email) {
                $message->from ( 'admin@abc-league.com', 'ABC League' );
                $message->to($email, 'Tak')->subject('Thank you for purchase account(s)!');
            });

            $code_del = Code::where('account_id', $acc[0]->id)->take($quantity)->delete();

            return view('Front.success', ['code' => $code   ]);
        } catch (\Exception $e) {
            Session::flash('fail-message', "Error! Please Try again.");
            return dd($e);
        }
    }

    public function payment($id)
    {

        $order = Order::where('order_id', $id)->first();


        $data = [];
        $data['items'] = [
            ['name' => $order->description, 'price' => $order->dev, 'desc'  => $order->description, 'qty' => 1,]
        ];
        $data['invoice_id'] = $order->id;
        $data['invoice_description'] = "Order #{$data['invoice_id']} Invoice";
        $data['return_url'] = route('payment.success');
        $data['cancel_url'] = route('payment.cancel');
        $total = 0;

        foreach($data['items'] as $item) {
            $total += $item['price']*$item['qty'];
        }
        $data['total'] = $total;



        $response = $this->provider->setExpressCheckout($data);

        $order->order_id = $response['TOKEN'];
        $order->status = 'W toku(czekamy na odpowiedź Paypala)';
        $order->payment = 'Paypal';


        $order->update();
//        return dd($response);
        return redirect($response['paypal_link']);
    }

    /**
     * Responds with a welcome message with instructions
     *
     * @return \Illuminate\Http\Response
     */
    public function cancel()
    {
        dd('Your payment is canceled. You can create cancel page here.');
    }


    public function success(Request $request)
    {
        $recurring = ($request->get('mode') === 'recurring') ? true : false;
        $order = Order::where('order_id', $request->token)->first();
        $token = $request->get('token');
        $PayerID = $request->get('PayerID');

        $cart = $this->getCheckoutData($request->token);


        $response = $this->provider->getExpressCheckoutDetails($request->token);
        if (in_array(strtoupper($response['ACK']), ['SUCCESS', 'SUCCESSWITHWARNING'])) {
            $payment_status = $this->provider->doExpressCheckoutPayment($cart, $token, $PayerID);




//            $acc = Account::where('name', $request->name )->get();


            $order = Order::where('order_id', $token)->first();
            $firstname = $response['FIRSTNAME'];
            $lastname = $response['LASTNAME'];
            $accc = Account::where('name', $order->description )->get();

            $code = Code::where('account_id', $accc[0]->id)->get()->take($order->quantity);

            $order = Order::where('order_id', $token)->first();
            $order->status = 'Zaplacono';
            $order->code = $code;
            $order->name = "$firstname $lastname";
            $order->update();

            $email = $order->email;
            Mail::send( 'mail.index',['code' => $code, 'email' => $order->email, 'name' => $order->description, 'order_id' => $request->token] ,function($message) use ($email) {
                $message->from ( 'admin@abc-league.com', 'ABC League' );
                $message->to($email, 'Tak')->subject('Thank you for purchase account(s)!');
            });
            $code_del = Code::where('account_id', $accc[0]->id)->take($order->quantity)->delete();

            return view('Front.success', ['code' => $code]);
        }

        dd('Something is wrong.');
    }

    public function txt($code)
    {
        return dd($code);
    }
    protected function getCheckoutData($hui)
    {
        $data = [];

        $order_id = 1;
        $orderR = Order::where('order_id', $hui)->first();

        $data['items'] = [
            [
                'name'  => $orderR->description,
                'price' => $orderR->dev,
                'qty'   => 1,
            ],

        ];
        $data['return_url'] = url('/');

        $data['invoice_id'] = $orderR->order_id;

        $data['invoice_description'] = "Order #$order_id Invoice";
        $data['cancel_url'] = url('/');

        $total = 0;
        foreach ($data['items'] as $item) {
            $total += $item['price'] * $item['qty'];
        }

        $data['total'] = $total;

        return $data;
    }
}
