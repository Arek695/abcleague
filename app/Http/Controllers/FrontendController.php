<?php

namespace App\Http\Controllers;

use App\AbcLeague\Interfaces\AccountsRepositoryInterface;
use App\AbcLeague\Interfaces\RegionsCheckInterface;
use App\Account;
use App\Checkout;
use App\Code;
use App\Mail\OrderShipped;
use App\Order;
use App\Region;
use App\Tos;
use CoinbaseCommerce\ApiClient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;


class FrontendController extends Controller
{
    public function generateRandomString($length = 10) {
        return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
    }

    public function __construct(AccountsRepositoryInterface $accountRepository, RegionsCheckInterface $regionsCheck) /* Lecture 13 FrontendRepositoryInterface */
    {
        $this->rC = $regionsCheck;
        $this->aR = $accountRepository;
    }

    public function change(Request $request)
    {
        DB::table('regions')->update(['currency' => $request->currency]);
        DB::table('accounts')->update(['currency' => $request->currency]);
        DB::table('start')->update(['name' => $request->name]);

        return redirect()->back();
    }

    public function index()
    {

        $eune = $this->rC->fetchEUNE();
        $euw = $this->rC->fetchEUW();
        $tr = $this->rC->fetchTR();
        $na = $this->rC->fetchNA();
        $lan = $this->rC->fetchLAN();
        $las = $this->rC->fetchLAS();
        $pbe = $this->rC->fetchPBE();
        $gr = $this->rC->fetchGR();
        $br = $this->rC->fetchBR();
        $oce = $this->rC->fetchOCE();
        $ru = $this->rC->fetchRU();
        $jp = $this->rC->fetchJP();
        $reviews = $this->aR->getAllReviews();
        $start =  DB::table('start')->first();
        $pyk = $start->name;
        $regions = $this->aR->getAllRegions();



        return view('Front.index',['regions' => $regions, 'reviews' => $reviews,
            'start'=>$pyk,
            'eune' => $eune,
            'euw' => $euw,
            'tr' => $tr,
            'na' => $na,
            'gr' => $gr,
            'oce' => $oce,
            'br' => $br,
            'lan'=> $lan,
            'las' => $las,
            'pbe' => $pbe,
            'ru' => $ru,
            'jp' => $jp
            ]);
    }


    public function generate(Request $request)
    {


        $name = hex2bin(request()->segment(count(request()->segments())));
        $order_id = $this->generateRandomString();

        $check = Checkout::where('name',$name)->first();
        ApiClient::init('8dda50fc-2c39-4470-b098-001499e8c2e1');
        $checkoutData = [
            'name' => 'ABC League',
            'description' =>  $check->name,
            'pricing_type' => 'fixed_price',
            'local_price' => [
                'amount' => $check->price,
                'currency' => $check->currency
            ],
            'requested_info' => ['name', 'email']
        ];
        $newCheckoutObj = \CoinbaseCommerce\Resources\Checkout::create($checkoutData);

        return view('Front.new_check', ['name' => $check->name, 'id' => $name, 'order_id' => $order_id,
            'quantity' => 1, 'price' => $check->price, 'currency'=> $check->currency, 'warranty_check' => 'No',
            'email' => 'office@abc-league.com',
            'pyk' => $newCheckoutObj->id,
            'description' => $check->description]);
    }

//    public function test()
//    {
//        $acc = Account::where('name', 'LAN Unranked Quality Smurf 50000+ BE Lvl 30')->get();
//        $code = Code::where('account_id', $acc[0]->id)->get()->take(2);
//        $email = 'arek.dob@o2.pl';
//        $order_id = 123;
//        Mail::send( 'mail.index',['code' => $code, 'name' => 'hej','order_id' => $order_id] ,function($message) use ($email) {
//            $message->from ( 'admin@abc-league.com', 'ABC League' );
//            $message->to($email, 'Tak')->subject('Thank you for purchase account(s)!');
//        });
//        $code_del = Code::where('account_id', $acc[0]->id)->take(2)->delete();
//
//        return view('Front.success', ['code' => $code]);
//    }
    public function regions()
    {
        $start =  DB::table('start')->first();
        $pyk = $start->name;
        $regions = $this->aR->getAllRegions();
        return view('Front.regions',['regions' => $regions, 'start'=>$pyk]);
    }
    public function accounts($name)
    {
        $id = Region::where('name', $name)->first();
        $count = Account::with(['regions'])
        ->where('region_id',$id->id)
        ->where('quantity','>=',1)
        ->count();
        $order_id = $this->generateRandomString();
        $accounts = $this->aR->getAccount($id->id);
        $regions = $this->aR->getAllRegions();
        $start =  DB::table('start')->first();
        $pyk = $start->name;

        return view('Front.accounts_neww',['accounts'=>$accounts], ['regions'=>$regions, 'start'=>$pyk, 'order_id' => $order_id, 'active' => $name, 'count' => $count]);
    }

    public function tos()
    {
        $start =  DB::table('start')->first();
        $pyk = $start->name;
        $tos = Tos::first();
        return view('Front.tos', ['tos' => $tos, 'start'=>$pyk]);
    }
}
