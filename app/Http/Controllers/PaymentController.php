<?php

namespace App\Http\Controllers;

use App\Order;
use Srmklive\PayPal\Services\ExpressCheckout;
use App\AbcLeague\Interfaces\AdminRepositoryInterface;
use App\Account;
use App\Code;
use Evilnet\Dotpay\DotpayManager;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Stripe\Charge;
use Stripe\Stripe;
use Illuminate\Support\Facades\DB;
use CoinbaseCommerce\ApiClient;
use CoinbaseCommerce\Resources\Checkout;




class PaymentController extends Controller
{
    protected $provider;
    public $PLN;
    public $description;
    static public $test;

    public function setPLN( $k )
    {
        return $this->PLN = $k;
    }
    public function setdescription($k)
    {
        return $this->description = $k;
    }


    public function setOrderID($id)
    {
        self::$test = $id;

    }
    public function getOrderID()
    {
        return self::$test;
    }
    public function __construct(AdminRepositoryInterface $adminRepository) /* Lecture 13 FrontendRepositoryInterface */
    {
        $this->provider = new ExpressCheckout();
        $this->aR = $adminRepository;
    }








}
