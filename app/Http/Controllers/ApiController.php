<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ApiController extends Controller
{
    protected function registered(Request $request, $user)
    {
        $user->generateToken();

        return response()->json(['data' => $user->toArray()], 201);
    }

    public function login(Request $request)
    {
        $loginDetails = $request->only('email', 'password');

        if(Auth::attempt($loginDetails)){
            return response()->json(['message' => 'login success']);
        }
        else{
            return response()->json(['message' => 'login failed']);

        }
    }

    public function register(Request $request)
    {
        $registerDetails = $request->only('name', 'email', 'password');

        // Here the request is validated. The validator method is located
        // inside the RegisterController, and makes sure the name, email
        // password and password_confirmation fields are required.
//        $this->validator($request->all())->validate();


        event(new Registered($user = $this->create($registerDetails)));

        return $this->registered($request, $user);
    }

    public function orders()
    {
        $test = Order::all();
        return response()->json($test);
    }
}
