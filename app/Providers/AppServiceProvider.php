<?php

namespace App\Providers;

use App\AbcLeague\Interfaces\AccountsRepositoryInterface;
use App\AbcLeague\Interfaces\AdminRepositoryInterface;
use App\AbcLeague\Interfaces\RegionsCheckInterface;
use App\AbcLeague\Repositories\AccountsRepository;
use App\AbcLeague\Repositories\AdminRepository;
use App\AbcLeague\Repositories\RegionsCheckRepository;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(AccountsRepositoryInterface::class, function(){
            return new AccountsRepository;
        });
        $this->app->bind(AdminRepositoryInterface::class, function(){
            return new AdminRepository();
        });
        $this->app->bind(RegionsCheckInterface::class, function(){
            return new RegionsCheckRepository();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

    }
}
