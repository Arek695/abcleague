<?php


namespace App\AbcLeague\Repositories;
use App\AbcLeague\Interfaces\AdminRepositoryInterface;
use App\Account;
use App\Code;
use App\Order;
use App\Region;
use App\Review;

class AdminRepository implements AdminRepositoryInterface
{


    public function getOrdersCount()
    {
        Return Order::count();
    }

    public function getOrders()
    {
        Return Order::orderBy('created_at', 'desc')->get();
    }

    public static function getAccountsCount($id)
    {
        Return Code::where('account_id',$id)->count();
    }

    public function getAllReviews()
    {
        return Review::all();
    }

    public function getAccount($id)
    {
        return Account::findOrFail($id);
    }

    public function getAllRegions()
    {
        Return Region::all();
    }

    public function getAllAccounts()
    {
        Return Account::orderBy('order', 'asc')->get();
    }

    public function getAccountsAllCount()
    {
        Return Account::count();
    }

    public static function convertIdToName($id)
    {
        $region = Region::where('id',$id)
            ->get('name');
        Return $region;
    }

    public function getReview($id)
    {
        return Review::findOrFail($id);
    }

    public function getRegion($id)
    {
        return Region::findOrFail($id);
    }

    public function checkRelationExist()
    {
        // TODO: Implement checkRelationExist() method.
    }

    public function prepare_order($desc, $email, $countrycode, $status, $payment, $warranty,$order_id, $price, $fee, $currency, $quantity, $src, $dev)
    {
        if(!empty($_SERVER['HTTP_CLIENT_IP'])){
            //ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            //ip pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        if($currency == "EUR"){


        $req_url = 'https://api.exchangerate-api.com/v4/latest/EUR';
        $response_json = file_get_contents($req_url);
        if(false !== $response_json) {

            try {

                $response_object = json_decode($response_json);

                $base_price = $price; // Your price in USD
                $PLN_price = round(($base_price * $response_object->rates->PLN), 2);

            }
            catch(Exception $e) {
                // Handle JSON parse error...
            }

        }
            $PLN_price = round(($base_price * $response_object->rates->PLN), 2);
        }
        if($currency == "USD"){


            $req_url = 'https://api.exchangerate-api.com/v4/latest/USD';
            $response_json = file_get_contents($req_url);
            if(false !== $response_json) {

                try {

                    $response_object = json_decode($response_json);

                    $base_price = $price; // Your price in USD
                    $PLN_price = round(($base_price * $response_object->rates->PLN), 2);

                }
                catch(Exception $e) {
                    // Handle JSON parse error...
                }

            }
            $PLN_price = round(($base_price * $response_object->rates->PLN), 2);
        }


        $xml = simplexml_load_file("http://www.geoplugin.net/xml.gp?ip=".$ip);

        $rng = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(10/strlen($x)) )),1,10);


        Return Order::insert([
            'description' => $desc,
            'dev' => $dev,
            'status' => $status,
            'email' => $email,
            'countrycode' => $xml->geoplugin_countryName,
            'payment' => $payment,
            'warranty' => $warranty,
            'order_id' => $order_id,
            'src' => $src,
            'price' => $price,
            'fee' => 0,
            'currency' => $currency,
            'PLN' => $PLN_price,
            'quantity' => $quantity,
        ]);
    }
}
