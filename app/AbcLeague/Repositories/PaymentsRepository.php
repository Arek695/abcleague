<?php


namespace App\AbcLeague\Repositories;
use App\AbcLeague\Interfaces\PaymentsRepositoryInterface;

class PaymentsRepository implements PaymentsRepositoryInterface
{
    public static function trash()
    {
        return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(10/strlen($x)) )),1,10);
    }
}
