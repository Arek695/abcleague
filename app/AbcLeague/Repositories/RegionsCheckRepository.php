<?php


namespace App\AbcLeague\Repositories;


use App\AbcLeague\Interfaces\RegionsCheckInterface;
use App\Account;
use App\Code;
use App\Region;

class RegionsCheckRepository implements RegionsCheckInterface
{

    public function fetchEUNE()
    {
        $region =  Region::where('name', 'EUNE')->get();
        $acc = Account::whereIn('region_id', $region)->get('id');
        if(Code::whereIn('account_id', $acc)->get()->isEmpty()){
            $region2 = Region::findOrFail($region[0]->id);
            $region2->avalible = 0;
            $region2->style = 'filtered';
            $region2->update();
        }
        Return Code::whereIn('account_id', $acc)->get();

    }

    public function fetchEUW()
    {
        $region =  Region::where('name', 'EUW')->get();
        $acc = Account::whereIn('region_id', $region)->get('id');
        if(Code::whereIn('account_id', $acc)->get()->isEmpty()){
            $region2 = Region::findOrFail($region[0]->id);
            $region2->avalible = 0;
            $region2->style = 'filtered';
            $region2->update();
        }
        Return Code::whereIn('account_id', $acc)->get();
    }

    public function fetchTR()
    {
        $region =  Region::where('name', 'TR')->get();
        $acc = Account::whereIn('region_id', $region)->get('id');
        if(Code::whereIn('account_id', $acc)->get()->isEmpty()){
            $region2 = Region::findOrFail($region[0]->id);
            $region2->avalible = 0;
            $region2->style = 'filtered';
            $region2->update();
        }
        Return Code::whereIn('account_id', $acc)->get();
    }

    public function fetchPBE()
    {
        $region =  Region::where('name', 'PBE')->get();
        $acc = Account::whereIn('region_id', $region)->get('id');
        if(Code::whereIn('account_id', $acc)->get()->isEmpty()){
            $region2 = Region::findOrFail($region[0]->id);
            $region2->avalible = 0;
            $region2->style = 'filtered';
            $region2->update();
        }
        Return Code::whereIn('account_id', $acc)->get();
    }

    public function fetchNA()
    {
        $region =  Region::where('name', 'NA')->get();
        $acc = Account::whereIn('region_id', $region)->get('id');
        if(Code::whereIn('account_id', $acc)->get()->isEmpty()){
            $region2 = Region::findOrFail($region[0]->id);
            $region2->avalible = 0;
            $region2->style = 'filtered';
            $region2->update();
        }
        Return Code::whereIn('account_id', $acc)->get();
    }

    public function fetchRU()
    {
        $region =  Region::where('name', 'RU')->get();
        $acc = Account::whereIn('region_id', $region)->get('id');
        if(Code::whereIn('account_id', $acc)->get()->isEmpty()){
            $region2 = Region::findOrFail($region[0]->id);
            $region2->avalible = 0;
            $region2->style = 'filtered';
            $region2->update();
        }
        Return Code::whereIn('account_id', $acc)->get();
    }

    public function fetchJP()
    {
        $region =  Region::where('name', 'JP')->get();
        $acc = Account::whereIn('region_id', $region)->get('id');
        if(Code::whereIn('account_id', $acc)->get()->isEmpty()){
            $region2 = Region::findOrFail($region[0]->id);
            $region2->avalible = 0;
            $region2->style = 'filtered';
            $region2->update();
        }
        Return Code::whereIn('account_id', $acc)->get();
    }

    public function fetchBR()
    {
        $region =  Region::where('name', 'BR')->get();
        $acc = Account::whereIn('region_id', $region)->get('id');
        if(Code::whereIn('account_id', $acc)->get()->isEmpty()){
            $region2 = Region::findOrFail($region[0]->id);
            $region2->avalible = 0;
            $region2->style = 'filtered';
            $region2->update();
        }
        Return Code::whereIn('account_id', $acc)->get();
    }

    public function fetchGR()
    {
        $region =  Region::where('name', 'GR')->get();
        $acc = Account::whereIn('region_id', $region)->get('id');
        if(Code::whereIn('account_id', $acc)->get()->isEmpty()){
            $region2 = Region::findOrFail($region[0]->id);
            $region2->avalible = 0;
            $region2->style = 'filtered';
            $region2->update();
        }
        Return Code::whereIn('account_id', $acc)->get();
    }

    public function fetchOCE()
    {
        $region =  Region::where('name', 'OCE')->get();
        $acc = Account::whereIn('region_id', $region)->get('id');
        if(Code::whereIn('account_id', $acc)->get()->isEmpty()){
            $region2 = Region::findOrFail($region[0]->id);
            $region2->avalible = 0;
            $region2->style = 'filtered';
            $region2->update();
        }
        Return Code::whereIn('account_id', $acc)->get();
    }

    public function fetchLAN()
    {
        $region =  Region::where('name', 'LAN')->get();
        $acc = Account::whereIn('region_id', $region)->get('id');
        if(Code::whereIn('account_id', $acc)->get()->isEmpty()){
            $region2 = Region::findOrFail($region[0]->id);
            $region2->avalible = 0;
            $region2->style = 'filtered';
            $region2->update();
        }
        Return Code::whereIn('account_id', $acc)->get();
    }

    public function fetchLAS()
    {
        $region =  Region::where('name', 'LAS')->get();
        $acc = Account::whereIn('region_id', $region)->get('id');
        if(Code::whereIn('account_id', $acc)->get()->isEmpty()){
            $region2 = Region::findOrFail($region[0]->id);
            $region2->avalible = 0;
            $region2->style = 'filtered';
            $region2->update();
        }
        Return Code::whereIn('account_id', $acc)->get();
    }


}
