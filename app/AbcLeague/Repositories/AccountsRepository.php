<?php


namespace App\AbcLeague\Repositories;


use App\AbcLeague\Interfaces\AccountsRepositoryInterface;
use App\Account;
use App\Region;
use App\Review;

class AccountsRepository implements AccountsRepositoryInterface
{

    public static function getSorted($id)
    {
        return Account::where('region_id',  $id)->orderBy('order', 'asc')->get();
    }


    public function getAllAccounts()
    {
        // TODO: Implement getAllAccounts() method.
    }

    public function getAccount($id)
    {
        return Account::with(['regions'])
            ->where('region_id',$id)
            ->where('quantity','>=',1)
            ->orderBy('order', 'asc')
            ->get();
    }

    public function getAccountsCount()
    {
        // TODO: Implement getAccountsCount() method.
    }

    public function getAllReviews()
    {
        return Review::all();
    }

    public function getRegion()
    {
        // TODO: Implement getRegion() method.
    }

    public function getAllRegions()
    {
        return Region::orderBy('avalible', 'desc')->get();
    }
}
