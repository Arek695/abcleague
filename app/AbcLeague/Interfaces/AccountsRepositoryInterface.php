<?php


namespace App\AbcLeague\Interfaces;


interface AccountsRepositoryInterface
{
    public static function getSorted($id);
    public function getAllAccounts();
    public function getAccount($id);
    public function getAllReviews();
    public function getAccountsCount();
    public function getRegion();
    public function getAllRegions();
}
