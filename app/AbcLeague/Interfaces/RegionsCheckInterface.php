<?php


namespace App\AbcLeague\Interfaces;


interface RegionsCheckInterface
{
    public function fetchEUNE();
    public function fetchEUW();
    public function fetchTR();
    public function fetchPBE();
    public function fetchNA();
    public function fetchRU();
    public function fetchJP();
    public function fetchBR();
    public function fetchGR();
    public function fetchOCE();
    public function fetchLAN();
    public function fetchLAS();
}
