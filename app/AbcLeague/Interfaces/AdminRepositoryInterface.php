<?php


namespace App\AbcLeague\Interfaces;


interface AdminRepositoryInterface
{
    public function getOrdersCount();
    public function getOrders();

    public static function  getAccountsCount($id);
    public function getAccountsAllCount();

    public function getAccount($id);
    public function getAllAccounts();

    public function getAllReviews();
    public function getReview($id);

    public function getRegion($id);
    public function getAllRegions();


    public static function convertIdToName($id);

    public function checkRelationExist();

}
