<?php

namespace App\Exports;

use App\Order;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class OrdersExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function headings(): array
    {
        return [
            'Obrót całowity w PLN',

        ];
    }

    public function collection()
    {
        return Order::selectRaw('sum(PLN) as PLN')
            ->where('status', 'Zaplacono')
            ->get();
    }
}
