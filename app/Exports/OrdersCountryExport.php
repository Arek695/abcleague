<?php

namespace App\Exports;

use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
class OrdersCountryExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */

    protected $id;

    function __construct($id) {
        $this->id = $id;
    }

    public function headings(): array
    {
        return [
            'Kraj',
            'Kwota PLN',
            'Fee',
            'Miesiąc'
        ];
    }
    public function collection()
    {


        return Order::groupBy('countrycode')
            ->selectRaw('countrycode, sum(PLN) as PLN, sum(fee) as fee, month(created_at) as month')
            ->whereMonth('created_at',$this->id)
            ->where('status', 'Zaplacono')
            ->get();
    }
}
