<?php

namespace App\Exports;

use App\Order;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class AllOrdersExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function headings(): array
    {
        return [
            'ID',
            'Nazwa konta',
            'Ilosc',
            'Status',
            'email',
            'Imie i Nazwisko',
            'Kraj',
            'Rodzaj płatności',
            'ID zamówienia',
            'Zapłacone dnia',
            'Cena',
            'Waluta',
            'Fee',
        ];
    }

    public function collection()
    {
        return Order::select('id','description','quantity','status','email','name','countrycode','payment','order_id', 'created_at', 'price', 'currency', 'fee')->where('status', 'Zaplacono')->get();
    }
}
