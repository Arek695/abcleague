<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::get('/asd', 'TestController@index');
Route::post('/change', 'FrontendController@change')->name('currency');

Route::get('/', 'FrontendController@index')->name('index');
Route::get('/accounts', 'FrontendController@regions')->name('regions');
Route::get('/accounts/{name}', 'FrontendController@accounts')->name('accounts');
Route::get('/tos', 'FrontendController@tos')->name('tos');

Route::post('/coupon/check', 'AjaxController@index');

Route::post('/pay/{order_id}', 'PaymentController@stripeIndex')->name('payment');
Route::post('/pay/stripe/{order_id}', 'PayPalController@stripePost')->name('stripe.post');
Route::get('/pay/paypal_get/{id}', 'PayPalController@payment')->name('paypal');
Route::get('/pay/paypal/cancel', 'PayPalController@cancel')->name('payment.cancel');
Route::get('/pay/paypal/success', 'PayPalController@success')->name('payment.success');
Route::get('/txt/{code}', 'PaymentController@txt')->name('payment.txt');
Route::post('/create_order', 'PayPalController@create_order')->name('create.order');



Route::get('/checkout/{text}', 'FrontendController@generate')->name('checkout');


Route::group(['prefix'=>'eqlee8dr32','middleware'=>'auth'],function() {
    Route::post('post-sortable','MenuController@update');

    Route::get('/', 'Admin\AdminController@index')->name('admin_index');
    Route::post('/show', 'Admin\AdminController@showw')->name('admin_showw');
    Route::get('/generate', 'Admin\AdminController@export');
    Route::get('/generate_spis', 'Admin\AdminController@export_spis');
    Route::get('/generate_country/{id}', 'Admin\AdminController@export_country');
    Route::get('/invoice/{id}', 'Admin\AdminController@invoice');
    Route::get('/delete_table/{id}', 'Admin\AdminController@delete')->name('admin_delete');

    Route::get('/customize', 'Admin\AdminController@customize')->name('admin_customize');
    Route::get('/sortowanie', 'Admin\AdminController@sortowanie')->name('admin_sort');
    Route::get('/customize/add', 'Admin\AdminController@customize_add')->name('add_checkout');
    Route::patch('/customize/store', 'Admin\AdminController@customize_add_store')->name('store_checkout');


    Route::get('/reviews', 'Admin\ReviewsController@reviews')->name('admin_reviews');
    Route::get('/reviews/add', 'Admin\ReviewsController@reviews_add')->name('admin_reviews_add');
    Route::get('/reviews/edit/{id}', 'Admin\ReviewsController@reviews_edit')->name('admin_reviews_edit');
    Route::patch('/reviews/edit/{id}', 'Admin\ReviewsController@edit_review_save')->name('edit_review_save');
    Route::get('/reviews/delete/{id}', 'Admin\ReviewsController@delete_review')->name('delete_review');
    Route::put('/reviews/add', 'Admin\ReviewsController@store_review')->name('store_review');

    Route::get('/regions', 'Admin\RegionsController@regions')->name('admin_regions');
    Route::get('/regions/add', 'Admin\RegionsController@regions_add')->name('admin_regions_add');
    Route::put('/regions/add', 'Admin\RegionsController@store_region')->name('admin_regions_add_store');
    Route::get('/regions/edit/{id}', 'Admin\RegionsController@regions_edit')->name('admin_regions_edit');
    Route::patch('/regions/edit/{id}', 'Admin\RegionsController@edit_region_save')->name('admin_regions_edit_store');
    Route::get('/regions/delete/{id}', 'Admin\RegionsController@delete_region')->name('delete_region');

    Route::get('/accounts', 'Admin\AccountsController@accounts_list')->name('admin_accounts_list');
    Route::get('/accounts/add', 'Admin\AccountsController@accounts_add')->name('admin_accounts_add');
    Route::put('/add', 'Admin\AccountsController@accounts_add_store')->name('admin_accounts_add_store');
    Route::get('/accounts/{id}', 'Admin\AccountsController@accounts')->name('admin_accounts');
    Route::patch('/accounts/edit/{id}', 'Admin\AccountsController@edit_accounts_store');
    Route::get('/delete/{id}', 'Admin\AccountsController@delete_account')->name('del');




    Route::get('/tos', 'Admin\AdminController@edit_tos')->name('tos_edit');
    Route::patch('/toss', 'Admin\AdminController@update_tos')->name('update_tos');
    Route::get('/coupon', 'Admin\AdminController@coupon')->name('coupon');


    Route::get('/livetable', 'LiveTable@index');
    Route::get('/livetable/fetch_data', 'LiveTable@fetch_data');
    Route::post('/livetable/add_data', 'LiveTable@add_data')->name('livetable.add_data');
    Route::post('/livetable/update_data', 'LiveTable@update_data')->name('livetable.update_data');
    Route::post('/livetable/delete_data', 'LiveTable@delete_data')->name('livetable.delete_data');

    Route::get('/coupon/livetable', 'CouponLiveTable@index');
    Route::get('/coupon/livetable/fetch_data', 'CouponLiveTable@fetch_data');
    Route::post('/coupon/livetable/add_data', 'CouponLiveTable@add_data')->name('Couponlivetable.add_data');
    Route::post('/coupon/livetable/update_data', 'CouponLiveTable@update_data')->name('Couponlivetable.update_data');
    Route::post('/coupon/livetable/delete_data', 'CouponLiveTable@delete_data')->name('Couponlivetable.delete_data');


});
