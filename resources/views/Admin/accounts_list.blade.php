@extends('layouts.admin')
@section('content')
<!-- Content Wrapper -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">
        <!-- Begin Page Content -->
        <div class="container-fluid">
            <!-- Begin Page Content -->
            <div class="container-fluid">


                <!-- Content Row -->

                <div class="row">
                    <div class="container-fluid">

                        <!-- Page Heading -->
                        <h1 class="h3 mb-2 text-gray-800">Konta</h1>
                        <p class="mb-4"> Lista kont.</p>

                        <!-- DataTales Example -->
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="table" width="100%" cellspacing="0">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Kolejność</th>
                                            <th>Region</th>
                                            <th>Nazwa</th>
                                            <th>Ilość</th>
                                            <th>Akcja</th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>ID</th>
                                            <th>Kolejność</th>
                                            <th>Region</th>
                                            <th>Nazwa</th>
                                            <th>Ilość</th>
                                            <th>Akcja</th>
                                        </tr>
                                        </tfoot>
                                        <tbody id="tablecontents">
                                        @foreach ($account as $acc)
                                            <tr class="row1" data-id="{{ $acc->order }}">
                                                <td>{{$acc->id}}</td>
                                                <td>{{$acc->order}}</td>
                                                <td>{{\App\AbcLeague\Repositories\AdminRepository::convertIdToName($acc->region_id)[0]->name}}</td>
                                                <td>{{$acc->name}}</td>
                                                <td>{{ \App\AbcLeague\Repositories\AdminRepository::getAccountsCount($acc->id)}}</td>
                                                <td>
                                                    <form action="/eqlee8dr32/accounts/{{$acc->id}}" method="GET">

                                                        <button type="submit" class="btn btn-warning">Edytuj</button>
                                                        <button  formaction="/eqlee8dr32/delete/{{$acc->id}}" class="btn btn-danger">Usuń</button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /.container-fluid -->
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $("#table").DataTable( {
                order: [[ 1, 'asc' ]],
                "pageLength": 50
            } );

            $( "#tablecontents" ).sortable({
                items: "tr",

                cursor: 'move',
                opacity: 0.6,
                update: function() {
                    sendOrderToServer();
                }
            });

            function sendOrderToServer() {
                var order = [];
                // var token = $('meta[name="csrf-token"]').attr('content');
                $('tr.row1').each(function(index,element) {
                    order.push({
                        id: $(this).attr('data-id'),
                        position: index+1
                    });
                });

                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "{{ url('/eqlee8dr32/post-sortable') }}",
                    data: {
                        order: order,
                        _token: "{{ csrf_token() }}",
                    },
                    success: function(response) {
                        if (response.status == "success") {
                            console.log(response);
                        } else {
                            console.log(response);
                        }
                    }
                });
            }
        });
    </script>
@endsection
