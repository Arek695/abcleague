@extends('layouts.admin')
@section('content')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="//cdn.ckeditor.com/4.13.1/basic/ckeditor.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">
        <!-- Begin Page Content -->
        <div class="container-fluid">
            <!-- Begin Page Content -->
            <div class="container-fluid">


                <!-- Content Row -->

                <div class="row">
                    <div class="container-fluid">

                        <!-- Page Heading -->
                        <h1 class="h3 mb-2 text-gray-800">Checkout</h1>
                        <p class="mb-4"> Dodaj checkout</p>

                        <!-- DataTales Example -->
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary"></h6>
                            </div>
                            <div class="card-body">
                                <div class="container box">
                                    <form role="form" action="{{route('store_checkout')}}" method="POST">
                                        @csrf
                                        @method('PATCH')

                                        <div class="card-body">
                                            <div class="form-group w-50 float-left">
                                                <label for="exampleInputEmail1">Tytuł (ważne żeby był unikalny i nie powtarzał się z innymi)</label>
                                                <input type="text" class="form-control"  name="name">
                                            </div>

                                            <div class="form-group w-25" style="clear: both;">
                                                Wybierz walute
                                                <select class="form-control" name="currency">
                                                    <option value="USD">USD</option>
                                                    <option value="EUR">EUR</option>
                                                    <option value="PLN">PLN</option>
                                                </select>
                                            </div>
                                            <div class="form-group w-5 float-left">
                                                <label for="price">Price </label>
                                                <input type="number" class="form-control" id="price" name="price" step="0.01" />
                                            </div>
                                            <div class="form-group" style="clear: both;">
                                                <label for="exampleInputEmail1">Description</label>
                                                <textarea name="description" id="editor1" rows="10" cols="80">

                                                </textarea>
                                                <script>
                                                    CKEDITOR.replace( 'editor1' );
                                                </script>

                                                {{--                                        </div>--}}
                                            </div>
                                            <div class="form-group" style="clear: both;">
                                                Wybierz kolor
                                                <select class="form-control" name="src">
                                                    <option value="Red">Capsules</option>
                                                    <option value="Blue">Blue Essence</option>
                                                    <option value="Pbe">PBE</option>
                                                    <option value="Platinum">Platinum</option>
                                                    <option value="Gold">Gold</option>
                                                </select>
                                            </div>
                                            <div class="form-group" style="clear: both;">
                                                Wybierz konto
                                                <select class="form-control" name="account_id">
                                                    @foreach($accounts as $acc)
                                                    <option value="{{$acc->id}}">{{$acc->name}} (kont na stanie: {{ \App\AbcLeague\Repositories\AdminRepository::getAccountsCount($acc->id)}} )</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                        </div>
                                        <!-- /.card-body -->

                                        <div class="card-footer">
                                            <button type="submit" class="btn btn-primary">Dodaj</button>
                                        </div>

                                    </form>

                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /.container-fluid -->
                </div>
        </div>
    </div>
</div>
    <script>
        function Round(n, k)
        {
            var factor = Math.pow(10, 2);
            return Math.round(n*factor)/factor;
        }

        function convert(){
            var eur = parseFloat(document.getElementById("EUR").value);
            var usd = document.getElementById("d");
            var wynik = parseFloat(eur*1.1);
            document.getElementById("USD").value = Round(wynik);
        }
    </script>
@endsection
