@extends('layouts.admin')
@section('content')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">
        <!-- Begin Page Content -->
        <div class="container-fluid">
            <!-- Begin Page Content -->
            <div class="container-fluid">


                <!-- Content Row -->
                <div class="row">

                    <!-- Earnings (Monthly) Card Example -->
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Zarobione w tym miesiącu(Euro)</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">€ {{$euro}}</div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-calendar fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Earnings (Monthly) Card Example -->
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Zarobione w tym miesiącu (Dolar)</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">${{$dollar}}</div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-calendar fa-2x text-gray-300"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>




                <!-- Content Row -->

                <div class="row">
                    <div class="container-fluid">

                        <!-- Page Heading -->
                        <h1 class="h3 mb-2 text-gray-800">Zamówienia</h1>
                        <p class="mb-4"> Lista zamówień złożona przez klientów.</p>
                        <form method="POST">@csrf<button class="btn btn-primary" formaction="/eqlee8dr32/show">Ukryj zamówienia rozpoczęte</button></form>

                        <!-- DataTales Example -->
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
                            </div>

                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                        <tr>
                                            <th>ID zamówienia</th>
                                            <th>Data</th>
                                            <th>Przedmiot</th>
                                            <th>Status</th>
                                            <th>Sposób płatności</th>
                                            <th>Gwarancja</th>
                                            <th>Email</th>
                                            <th>Code</th>
                                            <th>Cena</th>
                                            <th>Fee</th>
                                            <th>Waluta</th>
                                            <th>Usuń</th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>ID zamówienia</th>
                                            <th>Data</th>
                                            <th>Przedmiot</th>
                                            <th>Status</th>
                                            <th>Sposób płatności</th>
                                            <th>Gwarancja</th>
                                            <th>Email</th>
                                            <th>Code</th>
                                            <th>Cena</th>
                                            <th>Fee</th>
                                            <th>Waluta</th>
                                            <th>Usuń</th>
                                        </tr>
                                        </tfoot>
                                        <tbody>
                                        @foreach ($orders as $ord)
                                            <tr>
                                                <td><a href="eqlee8dr32/invoice/{{$ord->id}}">{{$ord->order_id}}</a></td>
                                                <td>{{$ord->created_at}}</td>
                                                <td>{{$ord->description}}</td>
                                                <td><span class="badge badge-info">{{$ord->status}}</span></td>
                                                <td>
                                                    <div class="sparkbar" data-color="#00a65a" data-height="20">{{$ord->payment}}</div>
                                                </td>
                                                <td><span class="badge badge-success text-center">{{$ord->warranty}}</span></td>
                                                <td>{{$ord->email}}</td>
                                                <td>{{$ord->countrycode}}</td>
                                                <td>{{$ord->price}}</td>
                                                <td>{{$ord->fee}}</td>
                                                <td>{{$ord->currency}}</td>
                                                <td><button  type="button" onclick="delete_acc{{$ord->id}}()" class="btn btn-danger ml-2">Usuń</button></td>
                                            </tr>
                                            <script>
                                                function delete_acc{{$ord->id}}() {
                                                    Swal.fire({
                                                        title: 'Are you sure?',
                                                        text: "tego się potem nie da odusunąć",
                                                        icon: 'warning',
                                                        showCancelButton: false,
                                                        showConfirmButton: false,

                                                        html: '<p>tego się potem nie da odusunąć</p><form method="GET"><button class="btn btn-danger mt-5" formaction="/eqlee8dr32/delete_table/{{$ord->id}}" type="submit">Usuń</button><form>',
                                                    }).then((result) => {
                                                        if (result.value) {

                                                        }
                                                    })
                                                }
                                            </script>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /.container-fluid -->
                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">

                        <a href="#" data-toggle="modal" data-target="#myModal" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generuj raport</a>
                    </div>

                </div>
        </div>
    </div>
</div>

        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Raport</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>

                    </div>
                    <div class="modal-body">
                        <p>Raport w formacie XLSX </p>
                        Wybierz miesiąc rozliczeniowy: <select onclick="myfun()" class="form-control" name="month" id="month">
                            <option value="">Wybierz miesiąc (miesiące dodają się automatycznie)</option>
                            @foreach($month as $m)
                            <option value="{{$m->month}}">{{$m->month}}.{{$m->year}}</option>
                                @endforeach
                        </select>
                    </div>
                    <div class="modal-footer">
                        <a id="test" href="" type="button" class="btn btn-default">Generuj miesięczne zestawienie dla krajów</a>
                        <a  href="/eqlee8dr32/generate" type="button" class="btn btn-default">Generuj obrót miesięczny </a>
                        <a  href="/eqlee8dr32/generate_spis" type="button" class="btn btn-default">Spis wszyskich zamówień </a>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
                <script>
                    function myfun() {
                        var e = document.getElementById("month");
                        var strUser = e.options[e.selectedIndex].value;
                        document.getElementById("test").href="/eqlee8dr32/generate_country/" + strUser;
                    }

                </script>
            </div>
        </div>

{{--        <script>--}}
{{--            $(document).ready(function() {--}}
{{--                $('#dataTable').DataTable( {--}}
{{--                    order: [[ 1, 'desc' ]],--}}
{{--                    "pageLength": 50--}}
{{--                } );--}}
{{--            } );--}}
{{--        </script>--}}
@endsection
