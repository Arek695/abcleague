@extends('layouts.admin')
@section('content')
    <script src="//cdn.ckeditor.com/4.13.1/basic/ckeditor.js"></script>

    <!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">
        <!-- Begin Page Content -->
        <div class="container-fluid">
            <!-- Begin Page Content -->
            <div class="container-fluid">


                <!-- Content Row -->

                <div class="row">
                    <div class="container-fluid">

                        <!-- Page Heading -->
                        <h1 class="h3 mb-2 text-gray-800">Edytor Tosu</h1>


                        <!-- DataTales Example -->
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">

                            </div>
                            <div class="card-body">
                                <div class="form-group" style="clear: both;">
                                    <form action="{{route('update_tos')}}" method="POST">
                                    @csrf
                                        @method('PATCH')

                                    <label for="exampleInputEmail1">Wersja pokazowa, jeszcze nie można edytować</label>


                                    <textarea name="description" id="editor1"  rows="50" >
                                                    {{$tos->text}}
                                                </textarea>
                                    <script>
                                        CKEDITOR.replace( 'editor1' );
                                        CKEDITOR.config.height = 900;
                                    </script>
                                        <input type="submit" class="btn btn-primary" value="Edytuj"/>
                                    </form>
                                    {{--                                        </div>--}}
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /.container-fluid -->
                </div>
        </div>
    </div>
</div>
@endsection
