@extends('layouts.admin')
@section('content')
<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">
        <!-- Begin Page Content -->
        <div class="container-fluid">
            <!-- Begin Page Content -->
            <div class="container-fluid">


                <!-- Content Row -->

                <div class="row">
                    <div class="container-fluid">

                        <!-- Page Heading -->
                        <h1 class="h3 mb-2 text-gray-800">Własne checkouty</h1>
                        <p class="mb-4"> Lista chechoutów.</p>

                        <!-- DataTales Example -->
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <a href="{{route('add_checkout')}}" class="btn btn-primary" >Stwórz checkout</a>

                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Nazwa</th>
                                            <th>Cena</th>
                                            <th>Waluta</th>
                                            <th>Info</th>
                                            <th>Akcja</th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>ID</th>
                                            <th>Nazwa</th>
                                            <th>Cena</th>
                                            <th>Waluta</th>
                                            <th>Info</th>
                                            <th>Akcja</th>
                                        </tr>
                                        </tfoot>
                                        <tbody>
                                        @foreach ($check as $ch)
                                            <tr>
                                                <td>{{$ch->id}}</td>
                                                <td>{{$ch->name}}</td>
                                                <td>{{$ch->price}}</td>
                                                <td>{{$ch->currency}}</td>
                                                <td>{{$ch->description}}</td>
                                                <td>
                                                    <form method="GET">
                                                        <button  type="submit" formaction="/checkout/{{bin2hex($ch->name)}}" class="btn btn-info">Prześlij link</button>
                                                        <button  type="submit" class="btn btn-danger">Usuń</button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /.container-fluid -->
                </div>
        </div>
    </div>
</div>
@endsection
