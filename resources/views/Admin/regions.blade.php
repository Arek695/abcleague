@extends('layouts.admin')
@section('content')
<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">
        <!-- Begin Page Content -->
        <div class="container-fluid">
            <!-- Begin Page Content -->
            <div class="container-fluid">


                <!-- Content Row -->

                <div class="row">
                    <div class="container-fluid">

                        <!-- Page Heading -->
                        <h1 class="h3 mb-2 text-gray-800">Regiony</h1>
                        <p class="mb-4"> Lista regionów.</p>

                        <!-- DataTales Example -->
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Pełna nazwa</th>
                                            <th>Skrócona nazwa</th>
                                            <th>Akcja</th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>ID</th>
                                            <th>Pełna nazwa</th>
                                            <th>Skrócona nazwa</th>
                                            <th>Akcja</th>
                                        </tr>
                                        </tfoot>
                                        <tbody>
                                        @foreach ($regions as $region)
                                            <tr>
                                                <td>{{$region->id}}</td>
                                                <td>{{$region->full_name}}</td>
                                                <td>{{$region->name}}</td>
                                                <td>
                                                    <form action="/eqlee8dr32/regions/edit/{{$region->id}}" method="GET">
                                                        <button type="submit" class="btn btn-warning">Edytuj</button>
                                                        <button type="submit"  formaction="/eqlee8dr32/regions/delete/{{$region->id}}" class="btn btn-danger">Usuń</button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /.container-fluid -->
                </div>
        </div>
    </div>
</div>
@endsection
