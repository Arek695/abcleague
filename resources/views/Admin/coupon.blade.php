@extends('layouts.admin')
@section('content')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="//cdn.ckeditor.com/4.13.1/basic/ckeditor.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">
        <!-- Begin Page Content -->
        <div class="container-fluid">
            <!-- Begin Page Content -->
            <div class="container-fluid">


                <!-- Content Row -->

                <div class="row">
                    <div class="container-fluid">

                        <!-- Page Heading -->
                        <h1 class="h3 mb-2 text-gray-800">Zarządzanie kuponami</h1>


                        <!-- DataTales Example -->
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary"></h6>
                            </div>
                            <div class="card-body">
                                <div class="container box">

                                    <form role="form" action="/eqlee8dr32/coupon/add" method="POST">
                                        @csrf
                                        @method('PUT')
                                        <div class="card-body">

                                            <h3 align="center">Zarządzanie kuponami </h3><br />
                                            <div class="panel panel-default">
                                            <p style="text-align: center">Zaznacz kupon jeśi nie chcesz aby ich ilość malała po użyciu</p>
                                                <div class="panel-body">
                                                    <div id="message"></div>
                                                    <div class="table-responsive">
                                                        <table class="table table-striped table-bordered">
                                                            <thead>
                                                            <tr>
                                                                <th>Nazwa kuponu</th>
                                                                <th>Zniżka</th>
                                                                <th>Ilość</th>
                                                                <th>Brak limitu użyć</th>
                                                                <th>Delete</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>

                                                            </tbody>
                                                        </table>
                                                        {{ csrf_field() }}
                                                    </div>
                                                </div>
                                            </div>



                                        </div>
                                        <!-- /.card-body -->


                                    </form>

                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /.container-fluid -->
                </div>
        </div>
    </div>
</div>


    <script>
        $(document).ready(function(){

            fetch_data();

            function fetch_data()
            {
                $.ajax({
                    url:"/eqlee8dr32/coupon/livetable/fetch_data",
                    dataType:"json",
                    {{--data:{id: '{{$account->id}}' },--}}
                    success:function(data)
                    {
                        var id = 1;
                        var html = '';

                        for(var count=0; count < data.length; count++)
                        {
                            html +='<tr>';
                            html +='<td contenteditable class="column_name" data-column_name="first_name" data-id="'+data[count].id+'">'+data[count].first_name+'</td>';
                            html += '<td contenteditable class="column_name" data-column_name="last_name" data-id="'+data[count].id+'">'+data[count].last_name+'%</td>';
                            html += '<td contenteditable class="column_name" data-column_name="quantity" data-id="'+data[count].id+'">'+data[count].quantity+'</td>';
                            html += '<td contenteditable class="column_name" data-column_name="limit" data-id="'+data[count].id+'"><input type="checkbox" name="limit" '+data[count].limit+'></td>';
                            html += '<td><button type="button" class="btn btn-danger btn-xs delete" id="'+data[count].id+'">Delete</button></td></tr>';
                        }
                        html += '<tr>';
                        html += '<td contenteditable id="first_name"></td>';
                        html += '<td contenteditable id="last_name"></td>';
                        html += '<td contenteditable id="quantity"></td>';
                        html += '<td contenteditable id="limit"><input type="checkbox" name="limit"></td>';
                        html += '<td><button type="button" class="btn btn-success btn-xs" id="add">Add</button></td></tr>';
                        $('tbody').html(html);
                    }
                });
            }

            var _token = $('input[name="_token"]').val();

            $(document).on('click', '#add', function(){

                var first_name = $('#first_name').text();
                var last_name = $('#last_name').text();
                var quantity = $('#quantity').text();
                debugger;
                var limit = "";
                if(document.getElementById("limit").checked == 'true'){
                    var limit = "checked";
                }else{
                    var limit = "";
                }

                if(first_name != '' && last_name != '')
                {
                    $.ajax({
                        url:"{{ route('Couponlivetable.add_data') }}",
                        method:"POST",
                        data:{first_name:first_name, last_name:last_name, quantity:quantity, limit:limit, _token:_token},
                        success:function(data)
                        {
                            $('#message').html(data);
                            fetch_data();
                        }
                    });
                }
                else
                {
                    $('#message').html("<div class='alert alert-danger'>Both Fields are required</div>");
                }
            });

            $(document).on('blur', '.column_name', function(){
                var column_name = $(this).data("column_name");
                var column_value = $(this).text();
                var id = $(this).data("id");

                if(column_value != '')
                {
                    $.ajax({
                        url:"{{ route('Couponlivetable.update_data') }}",
                        method:"POST",
                        data:{column_name:column_name, column_value:column_value, id:id, _token:_token},
                        success:function(data)
                        {
                            $('#message').html(data);
                        }
                    })
                }
                else
                {
                    $('#message').html("<div class='alert alert-danger'>Enter some value</div>");
                }
            });

            $(document).on('click', '.delete', function(){
                var id = $(this).attr("id");
                if(confirm("Are you sure you want to delete this records?"))
                {
                    $.ajax({
                        url:"{{ route('Couponlivetable.delete_data') }}",
                        method:"POST",
                        data:{id:id, _token:_token},
                        success:function(data)
                        {
                            $('#message').html(data);
                            fetch_data();
                        }
                    });
                }
            });


        });
    </script>
@endsection
