@extends('layouts.admin')
@section('content')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="//cdn.ckeditor.com/4.13.1/basic/ckeditor.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">
        <!-- Begin Page Content -->
        <div class="container-fluid">
            <!-- Begin Page Content -->
            <div class="container-fluid">


                <!-- Content Row -->

                <div class="row">
                    <div class="container-fluid">

                        <!-- Page Heading -->
                        <h1 class="h3 mb-2 text-gray-800">Konta</h1>
                        <p class="mb-4"> Zarzadzanie kontami.</p>

                        <!-- DataTales Example -->
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary"></h6>
                            </div>
                            <div class="card-body">
                                <div class="container box">
                                    <button  onclick="convert()" class="btn btn-primary">Convert</button>
                                    <p class="waluta">Obecnie kurs wynosi {{$waluta}}, aktualizowany co 30 minut.</p>
                                    <input type="hidden" id="waluta" value="{{$waluta}}"/>
                                    <form role="form" action="/eqlee8dr32/accounts/edit/{{$account->id}}" method="POST">
                                        @csrf
                                        @method('PATCH')

                                        <div class="card-body">
                                            <div class="form-group w-50 float-left">
                                                <label for="exampleInputEmail1">Title</label>
                                                <input type="text" class="form-control"  name="name" placeholder="{{$account->name}}" value="{{$account->name}}">
                                            </div>
                                            <div class="w-100"></div>


                                            <div class="form-group w-5 ml-5 float-left">
                                                <label for="exampleInputPassword1">Price EUR</label>
                                                <input type="text" class="form-control" id="EUR" name="price_eur"  placeholder="{{$account->price_eur}}" value="{{$account->price_eur}}" step="0.01" />
                                            </div>
                                            <div class="form-group w-5 ml-5 float-left">
                                                <label for="exampleInputPassword1">Price USD</label>
                                                <input type="text" class="form-control" id="USD" name="price" placeholder="{{$account->price}}" value="{{$account->price}}" step="0.01" >

                                            </div>

                                            <div class="form-group w-50 float-left">
                                                <label for="example1">Slug(skrócona nazwa widoczna w checkoutcie)</label>
                                                <input type="text" class="form-control"  name="slug" placeholder="{{$account->slug}}" value="{{$account->slug}}">
                                            </div>




                                            <div class="form-group" style="clear: both;">
                                                <label for="exampleInputEmail1">Description</label>


                                                <textarea name="description" id="editor1" rows="10" cols="80">
                                                    {{$account->description}}
                                                </textarea>
                                                <script>
                                                    CKEDITOR.replace( 'editor1' );
                                                </script>

                                                {{--                                        </div>--}}
                                            </div>

                                            <div class="panel panel-default">
                                                <div class="form-group">
                                                    <label for="exampleFormControlTextarea1">Dodawanie kont</label>
                                                    <textarea class="form-control" name="konta" id="exampleFormControlTextarea1" rows="8">@foreach($code as $c){{$c->code}}
@endforeach</textarea>
                                                </div>
                                            </div>

                                            <p>Ilosć kodów: {{count($code)}}</p>
                                            <div class="form-group" style="clear: both;">
                                                Wybierz kolor
                                                <select class="form-control" name="src">
                                                    <option value="{{$account->src}}" selected>{{$account->src}}</option>
                                                    <option value="Red">Capsules</option>
                                                    <option value="Blue">Blue Essence</option>
                                                    <option value="Pbe">PBE</option>
                                                    <option value="Platinum">Platinum</option>
                                                    <option value="Gold">Gold</option>
                                                </select>
                                                Kolejność: <input type="text" class="form-control"  name="order" placeholder="{{$account->order}}" value="{{$account->order}}"  >
                                            </div>



                                        </div>
                                        <!-- /.card-body -->

                                        <div class="card-footer">
                                            <button type="submit" class="btn btn-primary">Edit</button>
                                        </div>

                                    </form>

                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /.container-fluid -->
                </div>
        </div>
    </div>
</div>
    <script>
        function Round(n, k)
        {
            var factor = Math.pow(10, 2);
            return (Math.round(n * 4) / 4).toFixed(2);
        }

        function convert(){
            var eur = parseFloat(document.getElementById("EUR").value);
            var usd = document.getElementById("d");
            var przelicznik = parseFloat(document.getElementById("waluta").value);

            var wynik = parseFloat(eur*przelicznik);
            document.getElementById("USD").value = Round(wynik);
        }
    </script>
@endsection
