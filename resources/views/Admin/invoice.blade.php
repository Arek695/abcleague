@extends('layouts.admin')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" style="width: 100%">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Zamówienie</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="/eqlee8dr32">Powrót</a></li>

                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="callout callout-info">
                            <h5><i class="fas fa-info"></i> Notka:</h5>
                            Tą stronę będzie można niedługo wydrukować
                        </div>


                        <!-- Main content -->
                        <div class="invoice p-3 mb-3">
                            <!-- title row -->
                            <div class="row">
                                <div class="col-12">
                                    <h4>
                                        <i class="fas fa-globe"></i> ABC League

                                    </h4>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- info row -->
                            <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                    Spzedający
                                    <address>
                                        <strong>L&P</strong><br>
                                        NIP 9542809607<br>
                                        Poland, Katowice, Łabędzia 4<br>
                                        Email: contact@abc-league.com
                                    </address>
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    Nabywca
                                    <address>
                                        <strong>Imię i nazwisko: {{$invoice->name}}</strong><br>
                                        Kraj: {{$invoice->countrycode}}<br>
                                        Email: {{$invoice->email}}<br>
                                    </address>
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    <b>ID zamówienia {{$invoice->id}} </b><br>
                                    <br>
                                    <b>Zamówienie numer:</b> {{$invoice->order_id}}<br>
                                    <b>Data płatności:</b> {{$invoice->created_at}}<br>
                                    <b>Account:</b> 968-34567
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->

                            <!-- Table row -->
                            <div class="row">
                                <div class="col-12 table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Produkt</th>
                                            <th>Kod</th>
                                            <th>Gwarancja</th>
                                            <th>Kwota zapłacona</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>{{$invoice->description}}</td>
                                            @if(empty($code))
                                                <td>Niezakupiono (kod nie wysłany)</td>
                                            @else
                                            <td>
                                               {{$code}}
                                            </td>
                                            @endif
                                            <td>{{$invoice->warranty}}</td>
                                            @if($invoice->status == 'Płatność rozpoczęta')
                                                <td>Płatność niezakończona. Oczekiwanie na zapłacenie</td>
                                            @else
                                            <td>{{$invoice->price}}</td>
                                                @endif
                                        </tr>

                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->

                            <div class="row">
                                <!-- accepted payments column -->
                                <div class="col-6">
                                    <p class="lead">Zapłacono przez:</p>
                                    @if($invoice->status == 'Płatność rozpoczęta')
                                        <p>Płatność niezakończona. Oczekiwanie na zapłacenie</p>
                                    @else
                                        @if($invoice->payment == "Stripe")
                                    <img class="w-50" src="{{asset('images/Payments/stripe.png')}}" alt="Visa">
                                            @else
                                            <img class="w-50" src="{{asset('images/Payments/Paypal.png')}}" alt="Visa">
@endif
                                        @endif



                                </div>
                                <!-- /.col -->
                                <div class="col-6">
                                    <p class="lead">Dokonano płatności dnia: {{$invoice->created_at}} </p>

                                    <div class="table-responsive">
                                        <table class="table">
                                            <tr>
                                                <th style="width:50%">Cena całkowita:</th>
                                                <td>{{$invoice->price}} {{$invoice->currency}}</td>
                                            </tr>
{{--                                            <tr>--}}
{{--                                                <th>Prowizja (fee)</th>--}}
{{--                                                <td>{{$invoice->fee}} {{$invoice->currency}}</td>--}}
{{--                                            </tr>--}}
                                            <tr>
                                                <th>Prowizja (fee)</th>
                                                <td>{{$invoice->fee}} {{$invoice->currency}}</td>
                                            </tr>
                                            <tr>
                                                <th>Zarobek:</th>
                                                <td>{{$invoice->price - $invoice->fee}} {{$invoice->currency}}</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->

                            <
                        </div>
                        <!-- /.invoice -->
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>

@endsection
