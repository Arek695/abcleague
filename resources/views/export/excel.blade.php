<table>
    <thead>
    <tr>
        <th>Kraj</th>
        <th>Cena</th>
        <th>Fee</th>
    </tr>
    </thead>
    <tbody>
    @foreach($invoice as $in)
        <tr>
            <td>{{ $in->countrycode }}</td>
            <td>{{ $in->price }}</td>
            <td>{{ $in->fee }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
