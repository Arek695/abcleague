@extends('layouts.app')
@desktop
@section('content')
    <div id="preloder">
        <div class="loader"></div>
    </div>
    <style>
    .main{
        background-image: url("{{asset('images/bg_none.png')}}") !important;
    }
    #test li{
        list-style-image: url('../images/li.png') !important;
        margin-left: 30%;
        margin-right: 30%;
        font-family: Custom;
    }
    :not(li) > p {
        color: red;
        text-align: center;
        font-size: 20px;
    }
    .faq-list {
        margin-left: 30%;
        margin-right: 30%;
        font-family: Custom;
        list-style-type: none;
    }
    </style>
    <div class="flex-center position-ref full-height" id="test">
        <p class="up2" style="margin-top: 2%; font-size: 60px !important;">Account <b>TOS</b></p>
        {!! $tos->text !!}
    </div>

@endsection
@elsedesktop

@section('content')
    <div id="preloder">
        <div class="loader"></div>
    </div>
    <style>
        .main{
            background-image: url("{{asset('images/bg_none.png')}}") !important;
        }
        #test li{
            list-style-image: url('../images/li.png') !important;
            margin-left: 5%;
            margin-right: 5%;
            font-family: Custom;
        }
        :not(li) > p {
            color: red;
            text-align: center;
            font-size: 20px;
        }
        .faq-list {
            margin-left: 5%;
            margin-right: 5%;
            font-family: Custom;
            list-style-type: none;
        }
    </style>
    <div class="flex-center position-ref full-height" id="test">
        <p class="up2" style="margin-top: 2%; font-size: 60px !important;">Account <b>TOS</b></p>
        {!! $tos->text !!}
    </div>

@endsection
@enddesktop
