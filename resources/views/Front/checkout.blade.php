<!DOCTYPE html>
<!-- saved from url=(0407)https://checkout.stripe.com/pay/ppage_1GHaGhFKnpzPB0MXp8yvFXOv#fidkdWxOYHwnPyd1blpxYHZxWm9CPHY2XUhhVm9fQzxOYWgwYjA8f2lcYScpJ3dgY2B3d2B3SndsYmxrJz8nbXFxdXY%2FKip2cXdsdWArZmpoJyknaWpmZGlgJz9rcGlpKSdobGF2Jz9%2BJ2JwbGEnPyc2NzM9NWE9YCg0ZzQ2KDFnNmQoZzJjNig9MWEyNTEwMGRnN2cnKSdocGxhJz8nYzZhNGMzNGEoY2YzNCgxZjQ9KDw0NGcoNjE1NTY0MmdhMzM8JykndmxhJz8nYDE2MDAxNGEoZjw0ZigxYzMzKGc1ZGQoPT02NjMzNzIzNDJnJ3gpJ2dgcWR2Jz9eWHgl -->
<html lang="pl">

<head>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>


    <link rel="stylesheet" href="{{asset('vendor/fontawesome-free/css/all.css')}}">
    <script src="{{ asset('js/app.js') }}" defer></script>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="robots" content="noindex">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>ABCLeague - Payment</title>
</head>

<body>
<noscript>You need to enable JavaScript to run this app.</noscript>
<div id="root">
    <div class="App-Container App-Container flex-container justify-content-center">
        <div class="App App--singleItem" style="margin-top: 20px;">
            <div class="App-Overview">
                <header class="Header">
                    <div class="Header-Content Header-Content flex-container justify-content-space-between align-items-stretch">
                        <div class="Header-Business Header-Business flex-item width-auto">
                            <a class="Link Header-BusinessLink Link--primary" href="http://abc-league.webup-dev.pl" target="_self" aria-label="Poprzednia strona" title="Stripe Press">
                                <div style="position: relative;">
                                    <div class="flex-container align-items-center">
                                        <svg class="InlineSVG Icon Header-BusinessLink-arrow mr2 Icon--sm" focusable="false" width="12" height="12" viewBox="0 0 16 16">
                                            <path d="M3.417 7H15a1 1 0 0 1 0 2H3.417l4.591 4.591a1 1 0 0 1-1.415 1.416l-6.3-6.3a1 1 0 0 1 0-1.414l6.3-6.3A1 1 0 0 1 8.008 2.41z" fill-rule="evenodd"></path>
                                        </svg>
                                        <div class="flex-container align-items-center">
                                            <div class="HeaderImage--icon HeaderImage--icon flex-item width-fixed HeaderImage--icon flex-container justify-content-center align-items-center"></div>
                                            <div class="Header-BusinessLink-name-wrapper"><span class="Header-BusinessLink-label Text Text-color--gray800 Text-fontSize--14 Text-fontWeight--500"><span>Go back</span></span>
                                                <h1 class="Header-BusinessLink-name Text Text-color--gray800 Text-fontSize--14 Text-fontWeight--500 Text--truncate">ABC League</h1></div>
                                        </div>
                                        <div class="Tag mx2"><span class="Text Text-color--orange Text-fontSize--11 Text-fontWeight--700 Text-transform--uppercase">Live</span></div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </header>
                <div class="ProductSummary mt6">

                    <div class="ProductSummary-Info"><span class="Text Text-color--black Text-fontSize--16 Text-fontWeight--500">{{$name->name}}</span><span class="ProductSummary-TotalAmount Text Text-color--black Text-fontWeight--600 Text--tabularNumbers" id="ProductSummary-TotalAmount"><span>{{$name->price}} {{$name->currency}}</span></span>

                    </div>
                </div>
            </div>
            <div class="App-Payment">
                <div class="PaymentRequestOrHeader" style="height: 124px;">
                    <div class="PaymentHeaderContainer" style="opacity: 0; display: none;">
                        <div class="PaymentHeader">
                            <div class="Text Text-fontSize--20 Text-fontWeight--500"><span>Płatność kartą</span></div>
                        </div>
                    </div>
                    <div class="ButtonAndDividerContainer" style="opacity: 1; display: block;">
                        <div class="PaymentRequestButtonContainer StripeElement">
                            <div class="__PrivateStripeElement" style="margin: 0px !important; padding: 0px !important; border: none !important; display: block !important; background: transparent !important; position: relative !important; opacity: 1 !important;">
                                <img style="margin-left: 13%;width: 70%" src="../images/paypal_button.png"/></br>
                                {{--                                <img style="width: 70%" src="../images/paypal2_button.png"/>--}}
                                <img style="margin-left: 13%;width: 70%" src="../images/psc_button.png"/>

                                {{--                                <img style="width: 70%" src="../images/card_button.png"/>--}}
                                <input class="__PrivateStripeElement-input" aria-hidden="true" aria-label=" " autocomplete="false" maxlength="1" style="border: none !important; display: block !important; position: absolute !important; height: 1px !important; top: 0px !important; left: 0px !important; padding: 0px !important; margin: 0px !important; width: 100% !important; opacity: 0 !important; background: transparent !important; pointer-events: none !important; font-size: 16px !important;">
                            </div>
                        </div>
                        <div class="Divider">
                            <hr>
                            <p class="Divider-Text Text Text-color--gray400 Text-fontSize--14 Text-fontWeight--400"><span>or use Credit Card</span></p>
                        </div>
                    </div>

                </div>
                </br>
                <form role="form" action="/pay/stripe/{{$order_id}}" method="post" class="require-validation"
                      data-cc-on-file="false"
                      data-stripe-publishable-key="pk_live_ihzuWGm3A2rWUOixt7C4PSwD00fvsWe1Jj"
                      id="payment-form">
                    @csrf
                    @method('POST')
                    <input type="hidden" name="name" id="name" value="{{$name->name}}"/>
                    <input type="hidden" name="price" id="price" value="{{$name->price}}"/>
                    {{--                    <input type="hidden" name="email" id="email" value="{{$email}}"/>--}}
                    <input type="hidden" name="currency" id="currency" value="{{$name->currency}}"/>
                    <input type="hidden" name="order_id" id="order_id" value="{{$id}}"/>
                    <input type="hidden" name="quantity" id="quantity" value="1"/>
                    <div class='form-row row'>
                        <div class='col-xs-12 form-group card required ' style="border: none !important; background-color: transparent !important;">
                            <label style="background-color: transparent !important;" class='Text Text-color--black Text-fontSize--13 Text-fontWeight--500'>Card Number</label> <input
                                autocomplete='off' placeholder="ex. 1234 1234 1234 1234" class='form-control card-number' size='40'
                                type='text'>
                        </div>
                    </div>

                    <div class='form-row row'>
                        <div class='col-xs-12 col-md-4 form-group cvc required'>
                            <label class='Text Text-color--black Text-fontSize--13 Text-fontWeight--500'>CVC (three digits)</label> <input autocomplete='off'
                                                                                                                                           class='form-control card-cvc' placeholder='ex. 311' size='4'
                                                                                                                                           type='text'>
                        </div>
                        <div class='col-xs-12 col-md-4 form-group expiration required'>
                            <label class='Text Text-color--black Text-fontSize--13 Text-fontWeight--500'>Expiration Month</label> <input
                                class='form-control card-expiry-month' placeholder='MM' size='2'
                                type='text'>
                        </div>
                        <div class='col-xs-12 col-md-4 form-group expiration required'>
                            <label class='Text Text-color--black Text-fontSize--13 Text-fontWeight--500'>Expiration Year</label> <input
                                class='form-control card-expiry-year' placeholder='YYYY' size='4'
                                type='text'>
                        </div>
                    </div>

                    <div class='form-row row'>
                        <div class='col-md-12 error form-group hide'>
                            <div class='alert-danger alert'>Please correct the errors and try
                                again.</div>
                            <center><button class="btn btn-dark btn-lg btn-block" type="submit">Pay Now {{$name->price}} {{$name->currency}}</button></center>
                        </div>
                    </div>
            </div>
        </div>
        </div>
        </form>

        @if (Session::has('success'))
            <div class="alert alert-success text-center">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif


    </div>
    <footer class="App-Footer Footer">
    </footer>
</div>
</div>
</div>
</body>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>

<script type="text/javascript">
    $(function() {
        var $form         = $(".require-validation");
        $('form.require-validation').bind('submit', function(e) {
            var $form         = $(".require-validation"),
                inputSelector = ['input[type=email]', 'input[type=password]',
                    'input[type=text]', 'input[type=file]',
                    'textarea'].join(', '),
                $inputs       = $form.find('.required').find(inputSelector),
                $errorMessage = $form.find('div.error'),
                valid         = true;
            $errorMessage.addClass('hide');

            $('.has-error').removeClass('has-error');
            $inputs.each(function(i, el) {
                var $input = $(el);
                if ($input.val() === '') {
                    $input.parent().addClass('has-error');
                    $errorMessage.removeClass('hide');
                    e.preventDefault();
                }
            });

            if (!$form.data('cc-on-file')) {
                e.preventDefault();
                Stripe.setPublishableKey($form.data('stripe-publishable-key'));
                Stripe.createToken({
                    number: $('.card-number').val(),
                    cvc: $('.card-cvc').val(),
                    exp_month: $('.card-expiry-month').val(),
                    exp_year: $('.card-expiry-year').val()
                }, stripeResponseHandler);
            }

        });

        function stripeResponseHandler(status, response) {
            if (response.error) {
                $('.error')
                    .removeClass('hide')
                    .find('.alert')
                    .text(response.error.message);
            } else {
                // token contains id, last4, and card type
                var token = response['id'];
                // insert the token into the form so it gets submitted to the server
                $form.find('input[type=text]').empty();
                $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
                $form.get(0).submit();
            }
        }

    });
</script>
<link rel="stylesheet" href="{{asset('css/checkout.css')}}">
</html>
