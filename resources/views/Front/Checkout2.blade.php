<!DOCTYPE html>
<!-- saved from url=(0407)https://checkout.stripe.com/pay/ppage_1GDyR4FKnpzPB0MXWAxV1n5y#fidkdWxOYHwnPyd1blpxYHZxWm9CPHY2XUhhVm9fQzxOYWgwYjA8f2lcYScpJ3dgY2B3d2B3SndsYmxrJz8nbXFxdXY%2FKip2cXdsdWArZmpoJyknaWpmZGlgJz9rcGlpKSdobGF2Jz9%2BJ2JwbGEnPyc2NzM9NWE9YCg0ZzQ2KDFnNmQoZzJjNig9MWEyNTEwMGRnN2cnKSdocGxhJz8nYzZhNGMzNGEoY2YzNCgxZjQ9KDw0NGcoNjE1NTY0MmdhMzM8JykndmxhJz8nMDM3MmdgM2EoZzZkZygxMzxmKD0xYzYoNTMyZ2E2NDZgZzNmJ3gpJ2dgcWR2Jz9eWHgl -->
<html lang="pl" style="" >

<head>
    <style>


        #loader-wrapper {
            position: fixed;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            z-index: 1000;
            background: #ECF0F1;
            /* display: none; */
        }
        .load {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            width: 100px;
            height: 100px;
            /* display: none; */
        }

        .load hr {
            border: 0;
            margin: 0;
            width: 40%;
            height: 40%;
            position: absolute;
            border-radius: 50%;
            animation: spin 2s ease infinite;
        }

        .load :first-child {
            background: #19A68C;
            animation-delay: -1.5s;
        }

        .load :nth-child(2) {
            background: #F63D3A;
            animation-delay: -1s;
        }

        .load :nth-child(3) {
            background: #FDA543;
            animation-delay: -0.5s;
        }

        .load :last-child {
            background: #193B48;
        }

        @keyframes spin {
            0%, 100% {
                transform: translate(0)
            }
            25% {
                transform: translate(160%)
            }
            50% {
                transform: translate(160%, 160%)
            }
            75% {
                transform: translate(0, 160%)
            }
        }
    </style>
    <div id="loader-wrapper">
        <div class="load">
            <hr/><hr/><hr/><hr/>
        </div>
    </div>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>


    <link rel="stylesheet" href="{{asset('vendor/fontawesome-free/css/all.css')}}">
    <script src="{{ asset('js/app.js') }}" defer></script>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="robots" content="noindex">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>ABCLeague - Payment</title>
    <style>

    </style>
</head>

<body >
<script>

    $(window).on('load',function() {
        setTimeout(
            function() {
                $("#loader-wrapper").fadeOut();
            }, 2000);
    });

</script>
{{--<script>--}}

{{--    $(window).on('load', function() {--}}
{{--        $("#loader-wrapper").fadeOut();--}}
{{--    });--}}

{{--</script>--}}
<noscript>You need to enable JavaScript to run this app.</noscript>

<div id="root">
    <div class="App-Container App-Container flex-container justify-content-center">
        <div class="App App--singleItem" style="margin-top: 148px;">
            <div class="App-Overview">
                <header class="Header">
                    <div class="Header-Content Header-Content flex-container justify-content-space-between align-items-stretch">
                        <div class="Header-Business Header-Business flex-item width-auto">
                            <a class="Link Header-BusinessLink Link--primary" href="https://stripe.com/docs/payments/checkout?cancel=true#try-now" target="_self" aria-label="Poprzednia strona" title="Stripe Press">
                                <div style="position: relative;">
                                    <div class="flex-container align-items-center">
                                        <a href="https://abcleague.webup-dev.pl" class="Text-color--white">Back</a>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </header>
                <div class="ProductSummary mt6">
                    <div class="ProductSummary-ProductImageContainer">
                        <div class="ProductImage-container"><img src="../images/Accounts/test/{{$src}}.jpg" alt="Product" class="ProductImage-image"></div>
                    </div>
                    <div class="ProductSummary-Info"><span class="Text Text-color--white Text-fontSize--16 Text-fontWeight--500">{{$name}}</span><span class="ProductSummary-TotalAmount Text Text-color--white Text-fontWeight--600 Text--tabularNumbers" id="ProductSummary-TotalAmount"><span>{{$price}} {{$currency}}</span></span>
                        @if($warranty_check == 'Yes')
                        <span class="Text Text-color--white Text-fontSize--14 Text-fontWeight--500" id="ProductSummary-Description">Extended Warranty: {{$warranty_check}} </span>
                            @endif
                    </div>
                </div>
            </div>
            <style>
                .card{
                    border: 1px solid white !important;
                }
            </style>
            <div class="App-Payment">
                <div class="PaymentRequestOrHeader" id="PaymentRequestOrHeader" style="height: 124px;">
                    <div class="PaymentHeaderContainer" style="opacity: 0; display: none;">
                        <div class="PaymentHeader">
                            <div class="Text Text-fontSize--20 Text-color--white Text-fontWeight--500"><span>Płatność kartą</span></div>
                        </div>
                    </div>
                    <div class="ButtonAndDividerContainer" style="opacity: 1; display: block;">
                        <div class="PaymentRequestButtonContainer StripeElement">
                            <div class="__PrivateStripeElement" style="margin: 0px !important; padding: 0px !important; border: none !important; display: block !important; background: transparent !important; position: relative !important; opacity: 1 !important;">

                                <div style="padding-top: 15px;" class="FormFieldGroup-labelContainer FormFieldGroup-labelContainer flex-container justify-content-space-between">
                                    <label for="country"><span class="Text Text-color--white Text-fontSize--13 Text-fontWeight--500"><span>Choose your Payment method</span></span>
                                    </label>
                                </div>
                                <div class="accordion" id="accordionExample">
                                    <div class="card" style="background-color: transparent !important;">
                                        <div class="card-header" style="background-color: transparent !important;" id="headingOne">
                                            <h2 class="mb-0">
                                                <button class="btn btn-link" style="background-color: transparent !important;" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                    <img style="width: 29%" src="../images/Payments/Paypal.png"/>
                                                </button>
                                            </h2>
                                        </div>

                                        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                            <div class="card-body">
                                                <span class="Text Text-color--white Text-fontSize--13 Text-fontWeight--500" ><span >Press the button to redirect</span></span></br>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="card" style="background-color: transparent !important;">
                                        <div class="card-header" style="background-color: transparent !important;" id="headingTwo">
                                            <h2 class="mb-0">
                                                <button class="btn btn-link collapsed" style="background-color: transparent !important;" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                    <img style="width: 40%" src="../images/Payments/Psc.png"/>
                                                </button>
                                            </h2>
                                        </div>
                                        <div id="collapseTwo" class="collapse" style="background-color: transparent !important;" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                            <div class="card-body">
                                                <span class="Text Text-color--white Text-fontSize--13 Text-fontWeight--500" ><span >Press the button to redirect</span></span></br>
                                                <a href="https://abcleague.webup-dev.pl"><img class="SubmitButton-TextContainer" style="padding-top: 10px; align-content: center; margin-left: 25%; margin-right: 25%; width: 50%" src="../images/Payments/Psc.png"/>
                                                </a>                       </div>
                                        </div>
                                    </div>

                                                <form role="form" action="/pay/stripe/{{$order_id}}" method="post" class="require-validation"
                                                      data-cc-on-file="false"
                                                      data-stripe-publishable-key="pk_test_2MDYvlblVTBUul6SrKp8pjSt00OCwBd7EU"
                                                      id="payment-form">
                                                    @csrf
                                                    @method('POST')
                                                    <div class='form-row row'>
                                                        <div class='col-xs-12 form-group required'>
                                                            <label class='Text Text-color--white Text-fontSize--13 Text-fontWeight--500'>Name on Card</label></br> <input
                                                                class='form-control' size='40' type='text'>
                                                        </div>
                                                    </div>
                                                    <input type="hidden" name="name" id="name" value="{{$name}}"/>
                                                    <input type="hidden" name="price" id="price" value="{{$price}}"/>
                                                    <input type="hidden" name="email" id="email" value="{{$email}}"/>
                                                    <input type="hidden" name="currency" id="currency" value="{{$currency}}"/>
                                                    <input type="hidden" name="order_id" id="order_id" value="{{$order_id}}"/>
                                                    <input type="hidden" name="quantity" id="quantity" value="{{$quantity}}"/>
                                                    <div class='form-row row'>
                                                        <div class='col-xs-12 form-group card required text-white' style="border: none !important; background-color: transparent !important;">
                                                            <label style="background-color: transparent !important;" class='Text Text-color--white Text-fontSize--13 Text-fontWeight--500'>Card Number</label> <input
                                                                autocomplete='off' placeholder="ex. 1234 1234 1234 1234" class='form-control card-number' size='40'
                                                                type='text'>
                                                        </div>
                                                    </div>

                                                    <div class='form-row row'>
                                                        <div class='col-xs-12 col-md-4 form-group cvc required'>
                                                            <label class='Text Text-color--white Text-fontSize--13 Text-fontWeight--500'>CVC (three digits)</label> <input autocomplete='off'
                                                                                                                                                                           class='form-control card-cvc' placeholder='ex. 311' size='4'
                                                                                                                                                                           type='text'>
                                                        </div>
                                                        <div class='col-xs-12 col-md-4 form-group expiration required'>
                                                            <label class='Text Text-color--white Text-fontSize--13 Text-fontWeight--500'>Expiration Month</label> <input
                                                                class='form-control card-expiry-month' placeholder='MM' size='2'
                                                                type='text'>
                                                        </div>
                                                        <div class='col-xs-12 col-md-4 form-group expiration required'>
                                                            <label class='Text Text-color--white Text-fontSize--13 Text-fontWeight--500'>Expiration Year</label> <input
                                                                class='form-control card-expiry-year' placeholder='YYYY' size='4'
                                                                type='text'>
                                                        </div>
                                                    </div>

                                                    <div class='form-row row'>
                                                        <div class='col-md-12 error form-group hide'>
                                                            <div class='alert-danger alert'>Please correct the errors and try
                                                                again.</div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <center><button class="btn btn-dark btn-lg btn-block" type="submit">Pay Now {{$price}} {{$currency}}</button></center>
                                                        </div>
                                                    </div>

                                                </form>


                                </div>
                                </br>
                                <script>
                                    var $myGroup = $('#accordion');
                                    $myGroup.on('show.bs.collapse','.collapse', function() {
                                        $myGroup.find('.collapse.show').collapse('hide');
                                    });
                                </script>

                            </div>
                        </div>

                    </div>
                </div>



                @if (Session::has('success'))
                    <div class="alert alert-success text-center">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <p>{{ Session::get('success') }}</p>
                    </div>
                @endif


            </div>
            <footer class="App-Footer Footer">
            </footer>
        </div>
    </div>
</div>
</body>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>

<script type="text/javascript">
    $(function() {
        var $form         = $(".require-validation");
        $('form.require-validation').bind('submit', function(e) {
            var $form         = $(".require-validation"),
                inputSelector = ['input[type=email]', 'input[type=password]',
                    'input[type=text]', 'input[type=file]',
                    'textarea'].join(', '),
                $inputs       = $form.find('.required').find(inputSelector),
                $errorMessage = $form.find('div.error'),
                valid         = true;
            $errorMessage.addClass('hide');

            $('.has-error').removeClass('has-error');
            $inputs.each(function(i, el) {
                var $input = $(el);
                if ($input.val() === '') {
                    $input.parent().addClass('has-error');
                    $errorMessage.removeClass('hide');
                    e.preventDefault();
                }
            });

            if (!$form.data('cc-on-file')) {
                e.preventDefault();
                Stripe.setPublishableKey($form.data('stripe-publishable-key'));
                Stripe.createToken({
                    number: $('.card-number').val(),
                    cvc: $('.card-cvc').val(),
                    exp_month: $('.card-expiry-month').val(),
                    exp_year: $('.card-expiry-year').val()
                }, stripeResponseHandler);
            }

        });

        function stripeResponseHandler(status, response) {
            if (response.error) {
                $('.error')
                    .removeClass('hide')
                    .find('.alert')
                    .text(response.error.message);
            } else {
                // token contains id, last4, and card type
                var token = response['id'];
                // insert the token into the form so it gets submitted to the server
                $form.find('input[type=text]').empty();
                $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
                $form.get(0).submit();
            }
        }

    });
</script>
<link rel="stylesheet" href="{{asset('css/checkout.css')}}">
</html>
