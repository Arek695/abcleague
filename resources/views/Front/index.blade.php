@desktop

@extends('layouts.app')


@section('content')
    <script>
        function hover_eune() {
            document.getElementById("eune").setAttribute('src', '{{asset('images/Map/Hover/EUNE_n.png')}}');
        }

        function unhover_eune() {
            document.getElementById("eune").setAttribute('src', '{{asset('images/Map/Nowe1/EUNE.png')}}');
        }

        function hover_oce() {
            document.getElementById("oce").setAttribute('src', '{{asset('images/Map/Hover/OCE_n.png')}}');
        }

        function unhover_oce() {
            document.getElementById("oce").setAttribute('src', '{{asset('images/Map/Nowe1/OCE.png')}}');
        }

        function hover_euw() {
            document.getElementById("euw").setAttribute('src', '{{asset('images/Map/Hover/EUW_n.png')}}');
        }

        function unhover_euw() {
            document.getElementById("euw").setAttribute('src', '{{asset('images/Map/Nowe1/EUW.png')}}');
        }

        function hover_na() {
            document.getElementById("na").setAttribute('src', '{{asset('images/Map/Hover/NA_n.png')}}');
        }

        function unhover_na() {
            document.getElementById("na").setAttribute('src', '{{asset('images/Map/Nowe1/NA.png')}}');
        }

        function hover_pbe() {
            document.getElementById("pbe").setAttribute('src', '{{asset('images/Map/Hover/PBE_n.png')}}');
        }

        function unhover_pbe() {
            document.getElementById("pbe").setAttribute('src', '{{asset('images/Map/Nowe1/PBE.png')}}');
        }

        function hover_las() {
            document.getElementById("las").setAttribute('src', '{{asset('images/Map/Hover/LAS_n.png')}}');
        }

        function unhover_las() {
            document.getElementById("las").setAttribute('src', '{{asset('images/Map/Nowe1/LAS.png')}}');
        }

        function hover_lan() {
            document.getElementById("lan").setAttribute('src', '{{asset('images/Map/Hover/LAN_n.png')}}');
        }

        function unhover_lan() {
            document.getElementById("lan").setAttribute('src', '{{asset('images/Map/Nowe1/LAN.png')}}');
        }

        function hover_jp() {
            document.getElementById("jp").setAttribute('src', '{{asset('images/Map/Hover/JP_n.png')}}');
        }

        function unhover_jp() {
            document.getElementById("jp").setAttribute('src', '{{asset('images/Map/Nowe1/JP.png')}}');
        }

        function hover_ru() {
            document.getElementById("ru").setAttribute('src', '{{asset('images/Map/Hover/RUS_n.png')}}');
        }

        function unhover_ru() {
            document.getElementById("ru").setAttribute('src', '{{asset('images/Map/Nowe1/RUS.png')}}');
        }

        function hover_tr() {
            document.getElementById("tr").setAttribute('src', '{{asset('images/Map/Hover/TR_n.png')}}');
        }

        function unhover_tr() {
            document.getElementById("tr").setAttribute('src', '{{asset('images/Map/Nowe1/TR.png')}}');
        }
        function hover_br() {
            document.getElementById("br").setAttribute('src', '{{asset('images/Map/Hover/BR_n.png')}}');
        }
        function unhover_br() {
            document.getElementById("br").setAttribute('src', '{{asset('images/Map/Nowe1/BR.png')}}');
        }
    </script>
    <div id="preloder">
        <div class="loader"></div>
    </div>
<style>
    .main{
        background-image: url("{{asset('images/bg_tos.png')}}") !important;
        /*cursor: url(images/asd.png), pointer;*/
    }
    /*body{*/
    /*    cursor: url(images/asd.png), auto;*/

    /*}*/
    /*html{*/
    /*    cursor: url(images/asd.png), pointer;*/

    /*}*/

</style>
    <div id="cookieConsent">
        This website is using cookies. <a href="/tos" target="_blank">More info</a>. <a class="cookieConsentOK" style="margin-right: 10%;">That's Fine, i agree</a>
    </div>


    <div class="flex-center position-ref full-height">
        <p class="up">About<b>US</b><img class="ml-4" src="images/AboutUs.png"/></p>
        <p class="about">
            We are a reliable store that offers the cheapest League of Legends accounts for sale.</br>

            Customer satisfaction is the most important factor for us, for that reason we put the highest effort to make sure that our accounts have the best possible quality and a cheap affordable price.

            The purchase process is as simple as possible and can be done in just a few minutes and after you complete the payment the account will be instantly delivered to your e-mail.

            If you have any questions, feel free to message us. None of the queries remains unanswered.

        </p>

        <div class="container">
            <img class="w-100"src="images/Review.png"/>

            <div class="vc_row wpb_row vc_custom_1565195525613 porto-inner-container">
                <div class="porto-wrap-container container">
                    <div class="row"><div class="m-b-md vc_column_container col-md-12">
                            <div class="wpb_wrapper vc_column-inner">
                                <div class="vc_empty_space" style="height: 50px">
                                    <span class="vc_empty_space_inner"></span>
                                </div>
                                <div class="porto-u-heading" data-hspacer="no_spacer" data-halign="center" style="text-align:center">
                                    <div class="porto-u-main-heading">
                                        <h3 style="font-weight:600;color:white;font-size:40px;">We Offer</h3>
                                    </div>
                                    <div class="porto-u-sub-heading" style="color: #bcbcbc;font-size:25px;">For Your Pleasant Shopping Experience</div>
                                </div>
                                <div class="porto-separator  ">
                                    <div class="divider  align_center" style="background-image: -webkit-linear-gradient(left, transparent, grey, transparent); background-image: linear-gradient(to right, transparent, grey, transparent);">
                                        <i class="Simple-Line-Icons-heart"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--            <img class="w-100"src="images/Review.png"/>--}}
                <div class="vc_row wpb_row home-features vc_custom_1576615417534 no-padding text-center porto-inner-container">
                    <div class="porto-wrap-container container">
                        <div class="row align-items-center">
                            <div class="vc_column_container col-md-4 text-center">
                                <div class="wpb_wrapper vc_column-inner">
                                    <div class="porto-sicon-box style_3 top-icon">
                                        <div class="porto-sicon-top">
                                            <div id="porto-icon-10573819665e383f38b092c" class="porto-just-icon-wrapper" style="text-align:center;">
                                                <div class="porto-icon none " style="color: white;font-size:80px;display:inline-block;">
                                                    <img style="width: 30%" src="images/Icons/1.png">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="porto-sicon-header">
                                            <h4 class="porto-sicon-title" style="font-weight:600;font-size:25px;line-height:30px; color: white;">24/7 INSTANT DELIVERY</h4>
                                            <p style="font-weight:300;font-size:20px;line-height:20px;color:#839199;">After Successful Payment</p>
                                        </div> <!-- header --><div class="porto-sicon-description" style="">
                                            <h6 style="text-align: center;">
                                                <span style="font-size: 19px;  color: white;">Once you decide to purchase an account from us, it will be instantly delivered after you have completed your purchase on screen and via email. Our automated systems work 24/7, every day of the year.</span>
                                            </h6>
                                            <p>

                                            </p>
                                        </div> <!-- description -->
                                    </div> <!-- porto-sicon-box --></div>
                            </div>
                            <div class="vc_column_container col-md-4">
                                <div class="wpb_wrapper vc_column-inner">
                                    <div class="porto-sicon-box style_3 top-icon">
                                        <div class="porto-sicon-top">
                                            <div id="porto-icon-12417850005e383f38b104f" class="porto-just-icon-wrapper" style="text-align:center;">
                                                <div class="porto-icon none " style="color:white;font-size:80px;display:inline-block;">
                                                    <img class="w-25" src="images/Icons/2.png">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="porto-sicon-header">
                                            <h4 class="porto-sicon-title" style="font-weight:600;font-size:25px;line-height:30px; color: white;">Secure Payments</h4>
                                            <p style="font-weight:300;font-size:20px;line-height:20px;color:#839199;">100% Security For Our Customers</p>
                                        </div> <!-- header -->
                                        <div class="porto-sicon-description" style="">
                                            <h6 style="text-align: center;"><span style="font-size: 19px; color: white;">Our site is using SSL technology as well as PayPal, Bank transfer, Paysafecard, Credit Card for payment processing, and it is therefore secured.</span></h6>
                                            <p></p>
                                        </div> <!-- description -->
                                    </div> <!-- porto-sicon-box -->
                                </div>
                            </div>
                            <div class="vc_column_container col-md-4">
                                <div class="wpb_wrapper vc_column-inner">
                                    <div class="porto-sicon-box style_3 top-icon">
                                        <div class="porto-sicon-top">
                                            <div id="porto-icon-18593093105e383f38b18a1" class="porto-just-icon-wrapper" style="text-align:center;">
                                                <div class="porto-icon none " style="color:white;font-size:80px;display:inline-block;">
                                                    <img class="w-25" src="images/Icons/3.png">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="porto-sicon-header"><h4 class="porto-sicon-title" style="font-weight:600;font-size:25px;line-height:30px;  color: white;">NO HASSLE WARRANTY</h4><p style="font-weight:300;font-size:20px;line-height:20px;color:#839199;">Your Purchase Is Protected</p>
                                        </div> <!-- header -->
                                        <div class="porto-sicon-description" style="">
                                            <h6 style="text-align: center;"><span style="font-size: 19px; color: white;">One of the many things we provide is a hassle-free guarantee and outstanding support for all of our costumers. If something happens to your League of Legends account, you’re safe thanks to the guarantee!</span></h6>
                                            <p></p>
                                        </div> <!-- description -->
                                    </div> <!-- porto-sicon-box -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            <h2 class="text-center text-white mt-3"> Regions Avalible </h2>
            <style>.hotspot {
                    position: absolute;
                    /*border: 1px solid blue;*/
                    /*z-index: 9999;*/
                }

            </style>
                <style>
                    #counter {
                        position: relative;
                        display: inline;
                        /*cursor: url(images/cursor_2.png), auto !important;*/
                    }
                    #oce {
                         position:absolute;

                     }
                    #las {
                        position: absolute;
                    }
                    #lan {
                        position:absolute;

                    }
                    #pbe {
                        position:absolute;

                    }
                    #na {
                        position:absolute;

                    }
                    #euw {
                        position:absolute;

                    }
                    #eune {
                        position:absolute;

                    }
                    #ru {
                          position:absolute;

                      }
                    #tr {
                        position:absolute;

                    }
                    #jp {
                        position:absolute;

                    }
                    #br {
                        position:absolute;

                    }
                </style>
                <!-- Image Map Generated by http://www.image-map.net/ -->



                <div id="counter">
            <img style="width:99%; padding-top: 35px; top: 0; left: 0;" usemap="#image-map" src="{{asset('images/Map/MAP.png')}}"/>

            @if(!$oce->isEmpty())
               <img id="oce" style="width:100%; left: 0; padding-top: 35px;" src="{{asset('images/Map/Nowe1/OCE.png')}}">
                        <a href="/accounts/OCE"><div class="hotspot"  onmouseover="hover_oce()" onmouseout="unhover_oce()" style="z-index: 20; width: 120px; height: 155px; top: 145px; left: 880px"></div></a>
            @endif


            @if(!$las->isEmpty())
                <img id="las" style="width:100%; left: 0; padding-top: 35px; z-index: 10;" src="{{asset('images/Map/Nowe1/LAS.png')}}">
                        <a href="/accounts/LAS"><div class="hotspot"  onmouseover="hover_las()" onmouseout="unhover_las()" style="z-index: 20; width: 95px; height: 130px; top: 225px; left: 260px"></div></a>

                    @endif


            @if(!$lan->isEmpty())
            <img id="lan" style="width:100%; left: 0; padding-top: 35px; z-index: 9;" src="{{asset('images/Map/Nowe1/LAN.png')}}">
                        <a href="/accounts/LAN"><div class="hotspot"  onmouseover="hover_lan()" onmouseout="unhover_lan()" style="z-index: 20; width: 120px; height: 90px; top: 80px; left: 180px"></div></a>
                        <a href="/accounts/LAN"><div class="hotspot"  onmouseover="hover_lan()" onmouseout="unhover_lan()" style="z-index: 20; width: 40px; height: 50px; top: 170px; left: 260px"></div></a>

                    @endif


            @if(!$pbe->isEmpty())
            <img id="pbe" style="width:100%; left: 0; padding-top: 35px; z-index: 9;" src="{{asset('images/Map/Nowe1/PBE.png')}}">
                        <a href="/accounts/PBE"><div class="hotspot"  onmouseover="hover_pbe()" onmouseout="unhover_pbe()" style="z-index: 20;width: 130px; height: 115px; top: -40px; left: 170px"></div></a>

                    @endif


            @if(!$na->isEmpty())
                <img id="na" style="width:100%; left: 0; padding-top: 35px; z-index: 9;" src="{{asset('images/Map/Nowe1/NA.png')}}">
                        <a href="/accounts/NA"><div class="hotspot"  onmouseover="hover_na()" onmouseout="unhover_na()" style="z-index: 20;width: 415px; height: 255px; top: -300px; left: 10px"></div></a>

                    @endif


            @if(!$euw->isEmpty())
            <img id="euw" style="width:100%; left: 0; padding-top: 35px; z-index: 9;" src="{{asset('images/Map/Nowe1/EUW.png')}}">
                        <a href="/accounts/EUW"><div class="hotspot"  onmouseover="hover_euw()" onmouseout="unhover_euw()" style="z-index: 20;width: 80px; height: 95px; top: -75px; left: 480px"></div></a>

                    @endif


            @if(!$eune->isEmpty())
            <img id="eune" class="eune"  style="width:100%; left: 0; padding-top: 35px; z-index: 9;" src="{{asset('images/Map/Nowe1/EUNE.png')}}" >
                        <a href="/accounts/EUNE"><div class="hotspot"  onmouseover="hover_eune()" onmouseout="unhover_eune()" style="z-index: 20;width: 130px; height: 175px; top: -175px; left: 570px"></div></a>
                        <a href="/accounts/EUNE"><div class="hotspot"  onmouseover="hover_eune()" onmouseout="unhover_eune()" style="z-index: 20;width: 60px; height: 75px; top: -155px; left: 520px"></div></a>

                    @endif


            @if(!$ru->isEmpty())
            <img id="ru" style="width:100%; left: 0; padding-top: 35px; z-index: 9;" src="{{asset('images/Map/Nowe1/RUS.png')}}">
                        <a href="/accounts/RU"><div class="hotspot"  onmouseover="hover_ru()" onmouseout="unhover_ru()" style="z-index: 20;width: 350px; height: 160px; top: -190px; left: 705px"></div></a>

                    @endif

            @if(!$tr->isEmpty())
                <img id="tr" style="width:100%; left: 0; padding-top: 35px; z-index: 9;" src="{{asset('images/Map/Nowe1/TR.png')}}">
                        <a href="/accounts/TR"><div class="hotspot"  onmouseover="hover_tr()" onmouseout="unhover_tr()" style="z-index: 20;width: 85px; height: 30px; top: 5px; left: 590px"></div></a>

                    @endif

            @if(!$jp->isEmpty())
            <img id="jp" style="width:100%; left: 0; padding-top: 35px; z-index: 9;" src="{{asset('images/Map/Nowe1/JP.png')}}">
                        <a href="/accounts/JP"><div class="hotspot"  onmouseover="hover_jp()" onmouseout="unhover_jp()" style="z-index: 20;width: 75px; height: 90px; top: -20px; left: 920px"></div></a>
            @endif
                    @if(!$br->isEmpty())
                        <img id="br" style="width:100%; left: 0; padding-top: 35px; z-index: 9;" src="{{asset('images/Map/Nowe1/BR.png')}}">
                        <a href="/accounts/BR"><div class="hotspot"  onmouseover="hover_br()" onmouseout="unhover_br()" style="z-index: 20; width: 75px; height: 70px; top: 155px; left: 300px"></div></a>
                    @endif
    </div>
            <h2 class="text-center text-white mt-3"> Reviews </h2>
            <style>
                .flickity-button{
                    cursor: url(images/cursor_2.png), auto !important;
                }
            </style>
            <div class="gallery js-flickity" style="cursor: url(images/cursor_2.png), auto !important;" data-flickity-options='{ "wrapAround": true }'>
                @foreach($reviews as $rev)
                    <div class="gallery-cell" style="display: flex; flex-direction: column; cursor: url(images/cursor_2.png), auto !important;">
                        <p class="text-center mt-2" style="color: green !important; font-size: 28px;"><b>Positive</b></p>

                        <p class="text-center mt-3" style="color: black !important; font-size: 18px;">
                            {{$rev->tekst}}
                        </p>
                        <p class="text-center mt-auto" style="color: black !important;">{{$rev->author}}</p>

                    </div>
                @endforeach

            </div>
            </br>

{{--            <img class="w-100 mt-5"src="images/Discord_hr.png"/>--}}
            <p class="discord ">  <img class="ml-4 mr-4" src="images/Discord_2.png"/>Join Discord <img class="ml-4" src="images/Discord_2.png"/> </p>
            <p class="about2" >
                Discord server to stay tuned with new updates and giveaways.</br>

                Feel free join our Discord server to stay tuned with new updates and to have a chance to win free accounts in weekly giveaways.
            </p>
        </div>
        <!-- /.container -->
        <div class="text-center" style="font-size: 50px; color: white ">
            ><a href="https://discord.gg/vSRe8vT" style="color: #1B1C1E !important" target="_blank"> <img class="rounded mx-auto" style="width:11%; color: #1B1C1E !important" src="images/discord.png"/> </a><
            <a href="https://discord.gg/vSRe8vT" style="color: #1B1C1E !important" target="_blank"><p style="font-size: 18px !important;">Click here to join</p></a>
        </div>
        <style>

            a:hover img {
                border: none !important;

                border-bottom: 0px !important;
            }

        </style>

    </div>

@endsection
@elsedesktop


        @section('content')
            <div id="preloder">
                <div class="loader"></div>
            </div>
            <style>
                .filtered {
                    -webkit-filter: grayscale(100%);
                    filter: grayscale(100%);
                }
            </style>
            <div id="cookieConsent">
                <div id="closeCookieConsent">x</div>
                This website is using cookies. <a href="#" target="_blank">More info</a>. <a class="cookieConsentOK">That's Fine, i agree</a>
            </div>


            <div class="flex-center position-ref full-height">
                <p class="up" style="font-size: 80px;">About<b>US</b></p>
                <p class="about" style="margin-left: 10% !important; margin-right: 10% !important;">
                    We are a reliable store that offers the cheapest League of Legends accounts for sale.</br>

                    Customer satisfaction is the most important factor for us, for that reason we put the highest effort to make sure that our accounts have the best possible quality and a cheap affordable price.

                    The purchase process is as simple as possible and can be done in just a few minutes and after you complete the payment the account will be instantly delivered to your e-mail.

                    If you have any questions, feel free to message us. None of the queries remains unanswered.

                </p>


                    <div class="vc_row wpb_row vc_custom_1565195525613 porto-inner-container">
                        <div class="porto-wrap-container container">
                            <div class="row"><div class="m-b-md vc_column_container col-md-12">
                                    <div class="wpb_wrapper vc_column-inner">
                                        <div class="vc_empty_space" style="height: 50px">
                                            <span class="vc_empty_space_inner"></span>
                                        </div>
                                        <div class="porto-u-heading" data-hspacer="no_spacer" data-halign="center" style="text-align:center">
                                            <div class="porto-u-main-heading">
                                                <h3 style="font-weight:600;color:white;font-size:40px;">We Offer</h3>
                                            </div>
                                            <div class="porto-u-sub-heading" style="color: #bcbcbc;font-size:25px;">For Your Pleasant Shopping Experience</div>
                                        </div>
                                        <div class="porto-separator  ">
                                            <div class="divider  align_center" style="background-image: -webkit-linear-gradient(left, transparent, grey, transparent); background-image: linear-gradient(to right, transparent, grey, transparent);">
                                                <i class="Simple-Line-Icons-heart"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <img class="w-100"src="images/Review.png"/>
                        <div class="vc_row wpb_row home-features vc_custom_1576615417534 no-padding text-center porto-inner-container">
                            <div class="porto-wrap-container container">
                                <div class="row align-items-center">
                                    <div class="vc_column_container col-md-4 text-center">
                                        <div class="wpb_wrapper vc_column-inner">
                                            <div class="porto-sicon-box style_3 top-icon">
                                                <div class="porto-sicon-top">
                                                    <div id="porto-icon-10573819665e383f38b092c" class="porto-just-icon-wrapper" style="text-align:center;">
                                                        <div class="porto-icon none " style="color: white;font-size:80px;display:inline-block;">
                                                            <img style="width: 30%" src="images/Icons/1.png">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="porto-sicon-header">
                                                    <h4 class="porto-sicon-title" style="font-weight:600;font-size:25px;line-height:30px; color: white;">24/7 INSTANT DELIVERY</h4>
                                                    <p style="font-weight:300;font-size:20px;line-height:20px;color:#839199;">After Successful Payment</p>
                                                </div> <!-- header --><div class="porto-sicon-description" style="">
                                                    <h6 style="text-align: center;">
                                                        <span style="font-size: 19px;  color: white;">Once you decide to purchase an account from us, it will be instantly delivered after you have completed your purchase on screen and via email. Our automated systems work 24/7, every day of the year.</span>
                                                    </h6>
                                                    <p>

                                                    </p>
                                                </div> <!-- description -->
                                            </div> <!-- porto-sicon-box --></div>
                                    </div>
                                    <div class="vc_column_container col-md-4">
                                        <div class="wpb_wrapper vc_column-inner">
                                            <div class="porto-sicon-box style_3 top-icon">
                                                <div class="porto-sicon-top">
                                                    <div id="porto-icon-12417850005e383f38b104f" class="porto-just-icon-wrapper" style="text-align:center;">
                                                        <div class="porto-icon none " style="color:white;font-size:80px;display:inline-block;">
                                                            <img class="w-25" src="images/Icons/2.png">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="porto-sicon-header">
                                                    <h4 class="porto-sicon-title" style="font-weight:600;font-size:25px;line-height:30px; color: white;">Secure Payments</h4>
                                                    <p style="font-weight:300;font-size:20px;line-height:20px;color:#839199;">100% Security For Our Customers</p>
                                                </div> <!-- header -->
                                                <div class="porto-sicon-description" style="">
                                                    <h6 style="text-align: center;"><span style="font-size: 19px; color: white;">Our site is using SSL technology as well as PayPal ,Bank transfer ,Paysafecard ,Credit Card powered by Dotpay for payment processing, and it is therefore secured.</span></h6>
                                                    <p></p>
                                                </div> <!-- description -->
                                            </div> <!-- porto-sicon-box -->
                                        </div>
                                    </div>
                                    <div class="vc_column_container col-md-4">
                                        <div class="wpb_wrapper vc_column-inner">
                                            <div class="porto-sicon-box style_3 top-icon">
                                                <div class="porto-sicon-top">
                                                    <div id="porto-icon-18593093105e383f38b18a1" class="porto-just-icon-wrapper" style="text-align:center;">
                                                        <div class="porto-icon none " style="color:white;font-size:80px;display:inline-block;">
                                                            <img class="w-25" src="images/Icons/3.png">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="porto-sicon-header"><h4 class="porto-sicon-title" style="font-weight:600;font-size:25px;line-height:30px;  color: white;">NO HASSLE WARRANTY</h4><p style="font-weight:300;font-size:20px;line-height:20px;color:#839199;">Your Purchase Is Protected</p>
                                                </div> <!-- header -->
                                                <div class="porto-sicon-description" style="">
                                                    <h6 style="text-align: center;"><span style="font-size: 19px; color: white;">One of the many things we provide is a hassle-free guarantee and outstanding support for all of our costumers. If something happens to your League of Legends account, you’re safe thanks to the guarantee!</span></h6>
                                                    <p></p>
                                                </div> <!-- description -->
                                            </div> <!-- porto-sicon-box -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="container">
                            <img class="w-100"src="images/Review.png"/>
                            <h4 class="porto-sicon-title" style="text-align: center;font-weight:600;font-size:25px;line-height:30px; color: white;">Choose your server</h4>

                            <div class="row text-center mt-5">
                                @foreach ($regions as $region)
                                    <div class="col-lg-1 col-md-6 mb-4" style="width:25%">
                                        <div class="w-100">
                                            @if($region->avalible == 1)
                                                <a href="/accounts/{{$region->id}}">
                                                    <img class="card-img-top {{$region->style}}" style="text-align: left; " src="../images/Regions/{{$region->image}}" alt="">
                                                </a>
                                            @elseif($region->avalible == 0)
                                                <a >
                                                    <img class="card-img-top {{$region->style}}" style="text-align: left; " src="../images/Regions/{{$region->image}}" alt="">
                                                </a>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <h2 class="text-center text-white mt-3"> Reviews </h2>

                            <div class="gallery js-flickity" data-flickity-options='{ "wrapAround": true }'>
                                @foreach($reviews as $rev)
                                    <div class="gallery-cell" style="width: 100%;">
                                        <p class="text-center mt-2" style="color: green !important; font-size: 28px;"><b>Positive</b></p>

                                        <p class="text-center mt-3" style="color: black !important; font-size: 18px;">
                                            {{--                            {{$rev->tekst}}--}}
                                            {{$rev->tekst}}
                                        </p>
                                        <p class="text-center mt-1" style="color: black !important;">{{$rev->author}}</p>

                                    </div>
                                @endforeach

                            </div>
                        <img class="w-100 mt-5"src="images/Discord_hr.png"/>
                        <p class="discord ">  <img class="ml-4 mr-4" style="width: 12%!important;" src="images/Discord_2.png"/>Join Discord <img class="ml-4 w-25" style="width: 12%!important;" src="images/Discord_2.png"/> </p>
                        <p class="about2" style="margin-left: 10% !important; margin-right: 10% !important;" >
                            Discord server to stay tuned with new updates and giveaways.</br>

                            Feel free join our Discord server to stay tuned with new updates and to have a chance to win free accounts in weekly giveaways.
                        </p>
                    </div>
                    <!-- /.container -->
                    <div class="text-center" style="font-size: 50px; color:white;">
                        ><a href="https://discord.gg/vSRe8vT"> <img class="rounded mx-auto" style="width:11%;" src="images/discord.png"/> </a><
                        <a href="https://discord.gg/vSRe8vT"><p style="font-size: 18px !important;">Click here to join</p></a>
                    </div>

                </div>
                @endsection
@enddesktop
