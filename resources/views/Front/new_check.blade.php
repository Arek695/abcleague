<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Słynny bootsztrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <!-- Fonty xD -->
    <title>Chechout - ABCLeague</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/brands.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/fontawesome.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/regular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/solid.min.js"></script>
    <!-- Wincyj fontów bo po co instalować to npm-em -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/brands.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/fontawesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/regular.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/v4-shims.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/svg-with-js.min.css">

    <script src="https://commerce.coinbase.com/v1/checkout.js">
    </script>
<style>
    .container{
    z-index: 9;
    left: 0;
    right: 0;
    margin-top: -5%;
    }
</style>
    <!-- Moje scssy, nic trudnego serio -->
    <link rel="stylesheet/less" type="text/css" href="../../css/moje.less" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/less.js/3.9.0/less.min.js" ></script>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <!-- kuniec -->


</head>
<body>
{{--<a href="https://abc-league.com" class="text-white float-left mb-3"style="margin-left: 17%; margin-top: 5%;">--}}
{{--    <img class="rounded w-25 ml-3 " src="{{asset('images/check_logo.png')}}"/></a>--}}
<header class="bg-primary text-white">



</header>
<main role="main">
    <div id="app">
    <div class="container bg-light position-absolute " >
        <div class="row">
            <div class="col-sm lewy">
                <h4>{{$quantity}}x {{$name}}</h4>

                <div id="clear" style="clear: both;"></div>
{{--                <p class="mt-2 text-black-50 font-weight-bolder">Select a payment method</p>--}}
                <br>
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <h2 class="mb-0">
                            <button @click="swapComponent('stripe')"class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                <i class="far fa-credit-card fa-lg" ></i> Credit Card
                            </button>
                        </h2>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <h2 class="mb-0">
                            <button @click="swapComponent('paypal')" class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                <i class="fab fa-paypal fa-lg"></i> Paypal
                            </button>
                        </h2>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <h2 class="mb-0">
                            <button @click="swapComponent('paysafecard')" class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                <i class="fas fa-lock fa-lg"></i> PaySafeCard
                            </button>
                        </h2>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <h2 class="mb-0">
                            <button @click="swapComponent('coinbase')" class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                <i class="fab fa-bitcoin fa-lg"></i> Cryptocurrencies
                            </button>
                        </h2>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <h2 class="mb-0">
                            <button @click="swapComponent('bank')" class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                <i class="fas fa-university fa-lg"></i> Bank transfer
                            </button>
                        </h2>
                    </div>
                </div>
            </div>
            <div  class="col-sm prawy">
                <h5 class="text-black-50 mt-3">Total amount</h5>
                <p class=" price" style="font-size: 24px !important">{{$price}} {{$currency}}</p>

                <div :is="currentComponent"
                     :postroute="{{json_encode($order_id)}}"
                     :name="{{json_encode($name)}}"
                     :price="{{json_encode($price)}}"
                     :email="{{json_encode($email)}}"
                     :currency="{{json_encode($currency)}}"
                     :order_id="{{json_encode($order_id)}}"
                     :quantity="{{json_encode($quantity)}}"
                     :pyk="{{json_encode($pyk)}}"

                ></div>
            </div>
        </div>
        <div class="card mt-5">
            <div class="card-body">
{{--                <p class="font-weight-bold">Secured payment powered by:</p>--}}
                <div class="row ml-1 ">
                    <div class="col-xs-6 col-md-2 col-md-offset-1 mt-2">
                        <img class="img-responsive w-100 ml-1" src="{{asset('images/images/coinbase.png')}}" />
                    </div>
                    <div class="col-xs-6 col-md-2 mt-3">
                        <img class="img-responsive w-100 ml-1" src="{{asset('images/images/pp.png')}}" />
                    </div>

                    <div class="col-xs-6 col-md-2 mt-4">
                        <img class="img-responsive w-100 ml-1" src="{{asset('images/images/psc.png')}}" />
                    </div>
                    <div class="col-xs-6 col-md-2 mt-3">
                        <img class="img-responsive w-100 ml-1" src="{{asset('images/images/fondy.png')}}" />
                    </div>
                    <div class="col-xs-6 col-md-2 mt-3 ">
                        <img class="img-responsive w-75 ml-3" src="{{asset('images/payop.png')}}" />
                    </div>
                    <div class="col-xs-6 col-md-2 mt-1 ">
                        <img class="img-responsive w-50 ml-1" src="{{asset('images/images/stripe.png')}}" />
                    </div>

                    {{--                        <img class="w-25" src="{{asset('images/coinbase.png')}}"/>--}}
                    {{--                        <img class="w-25 mr-3" src="{{asset('images/pp.png')}}"/>--}}
                    {{--                        <img class="w-25 ml-3" src="{{asset('images/psc.png')}}"/>--}}
                    {{--                        <img class="w-25" src="{{asset('images/stripe.png')}}"/>--}}
                </div>
            </div>
        </div>

    </div>
</main>

</body>
<script src="https://commerce.coinbase.com/v1/checkout.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>

<script type="text/javascript" src="https://js.stripe.com/v2/"></script>

<script type="text/javascript">
    $(function() {
        var $form         = $(".require-validation");
        $('form.require-validation').bind('submit', function(e) {
            var $form         = $(".require-validation"),
                inputSelector = ['input[type=email]', 'input[type=password]',
                    'input[type=text]', 'input[type=file]',
                    'textarea'].join(', '),
                $inputs       = $form.find('.required').find(inputSelector),
                $errorMessage = $form.find('div.error'),
                valid         = true;
            $errorMessage.addClass('hide');

            $('.has-error').removeClass('has-error');
            $inputs.each(function(i, el) {
                var $input = $(el);
                if ($input.val() === '') {
                    $input.parent().addClass('has-error');
                    $errorMessage.removeClass('hide');
                    e.preventDefault();
                }
            });

            if (!$form.data('cc-on-file')) {
                e.preventDefault();
                Stripe.setPublishableKey($form.data('stripe-publishable-key'));
                Stripe.createToken({
                    number: $('.card-number').val(),
                    cvc: $('.card-cvc').val(),
                    exp_month: $('.card-expiry-month').val(),
                    exp_year: $('.card-expiry-year').val()
                }, stripeResponseHandler);
            }

        });

        function stripeResponseHandler(status, response) {
            if (response.error) {
                $('.error')
                    .removeClass('hide')
                    .find('.alert')
                    .text(response.error.message);
            } else {
                // token contains id, last4, and card type
                var token = response['id'];
                // insert the token into the form so it gets submitted to the server
                $form.find('input[type=text]').empty();
                $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
                $form.get(0).submit();
            }
        }

    });
</script>
<script src="{{asset('js/app.js')}}"></script>
</html>
