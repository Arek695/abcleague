
<!doctype html>
<html lang="en">
<head>
    <script src="//code.tidio.co/orgswuezax7hliotdaqjm7yw2bc0wswn.js" async></script>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        a:hover {
            cursor: url({{asset('images/cursor_2.png')}}), auto !important;
        }
        body{
            cursor: url({{asset('images/asd.png')}}), auto;

        }
        html{
            cursor: url({{asset('images/asd.png')}}), auto;

        }
        .frame-content{
            cursor: url(images/asd.png), auto !important;
        }

        .widgetLabel{
            cursor: url(images/asd.png), auto !important;
        }
        .moveFromRightLabel-enter-done
        {
            cursor: url(images/asd.png), auto !important;
        }
        #button{
            cursor: url({{asset('images/cursor_2.png')}}), auto !important;
        }
        span{
            cursor: url({{asset('images/cursor_2.png')}}), auto !important;
        }
    </style>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>ABC League</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <!-- Scripts -->

    <link rel="stylesheet" href="{{asset('vendor/fontawesome-free/css/all.css')}}">
    {{--    <script src="{{ asset('js/app.js') }}" defer></script>--}}
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" ></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <!-- Google Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">

    <!-- Styles -->

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/flickity.css')}}" media="screen">
    <link rel="stylesheet" href="{{asset('css/accounts.css')}}" media="screen">

    <nav class="navbar navbar-expand-md navbar-light bg-blue shadow-sm">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                <img class="rounded logo" style="width: 30%!important" src="{{asset('images/white-logo.png')}}"/>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    <li class="nav-item ">
                        <a class="nav-link" href="/accounts">Accounts</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/tos"><b>TOS</b></a>
                    </li>

                    <li class="nav-item">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="{{$start}}">
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <form method="POST" action="{{route('currency')}}" class="cyk" >@csrf @method('POST')<input type="hidden" class="cyk" name="currency" value="USD"><input type="hidden" class="cyk" name="name" value="{{asset('images/dolar_nowy.png')}}">
                                <button type="submit" class="dropdown-item cyk"  style="cursor: url(images/cursor_2.png), auto !important;;" href="#"><img class="cyk" src="{{asset('images/dolar_nowy.png')}}" />Dollar</button></form>
                            <form method="POST" action="{{route('currency')}}" class="cyk">@csrf @method('POST')<input type="hidden" class="cyk" name="currency" value="EUR"><input type="hidden" class="cyk" name="name" value="{{asset('images/euro_nowe.png')}}">
                                <button type="submit" class="dropdown-item cyk"  style="cursor: url(images/cursor_2.png), auto !important;;" href="#"><img class="cyk" src="{{asset('images/euro_nowe.png')}}" />Euro</button></form>


                        </div>
                    </li>
                </ul>
            </div>
        </div>

    </nav>

</head>
<body>
<div id="preloder">
    <div class="loader"></div>
</div>
<div class="main">

    <script>
        $(function() {
            $("#toggle.dropdown-menu li a").click(function() {
                $('#curent').text($(this).text() + 'Profile');
            });
        });
    </script>
    <style>

        .{{$active}}{
            -webkit-box-shadow: 5px -5px 5px 0px rgba(17,173,40,0.59);
            -moz-box-shadow: 5px -5px 5px 0px rgba(17,173,40,0.59);
            box-shadow: 5px -5px 5px 0px rgba(17,173,40,0.59);
        }

    </style>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <div class="flex-center position-ref full-height">

        <!-- Page Content -->
        <div class="container">
            <div class="row text-center mt-5 d-flex justify-content-center">
                @foreach ($regions as $region)
                <div class="col-lg-1 col-md-6 mb-4">
                    <div class="w-100">
                        @if($region->avalible == 1)
                        <a href="/accounts/{{$region->name}}">
                            <img class="card-img-top {{$region->style}} {{$region->name}}" style="text-align: left; " src="../images/Regions/{{$region->image}}" alt="">
                        </a>
                        @elseif($region->avalible == 0)
                        <a >
                            <img class="card-img-top {{$region->style}} " style="text-align: left; " src="../images/Regions/{{$region->image}}" alt="">
                        </a>
                        @endif
                    </div>
                </div>
                @endforeach
            </div>
            <!-- Jumbotron Header -->
            <img class="w-100" src="../images/Review.png"/>
            <!-- Page Features -->
            <div class="card-deck mb-3 text-center d-flex justify-content-center">
                @foreach ($accounts as $acc)
                @if($count < 3)
                <style>
                    .mykmyk{
                        margin-left: 12%;
                        margin-right: 12%;
                    }
                </style>
                @else
                <style>
                    .mykmyk{
                        margin-left: 0%;
                        margin-right: 0%;
                    }
                </style>
                @endif
                <div class=" col-lg-3 col-md-6 mb-4 mykmyk" >
                    <div class="card h-100" style="background-color: transparent" >
                        <div class="w-100" >
                            <a href="#">
                                @if(file_exists( public_path().'/images/Accounts/test/'.$acc->src.'.png' ))
                                <img class="card-img-top"  src="../images/Accounts/test/{{$acc->src}}.png" alt="">
                                @else
                                <img class="card-img-top" src="../images/Accounts/250.png" alt="">
                                @endif
                            </a>
                            <p class="tekst mt-3" style="text-shadow: 0 0 0.2em #696969; position: absolute !important; display: block; font-size: 21px; top: 70px; margin-left: 20%; left: -5px; text-align: center; align-content: center;">
                            </p>
                        </div>
{{--                        <hr style="height: 2px;background-color: {{$acc->color}}">--}}
                        <img class="mt-3 mb-1" src="../../images/{{$acc->color}}.png"/>
                        <div class="tekst " style="color: white !important; margin-top: 2%; font-size: 14.4px !important; ">
                            {!! $acc->description !!}
                        </div>

                        <a class=" mt-auto"  style="background-color:transparent" >
                            @if($acc->currency == 'USD') <p class="tekst mt-auto"  style="font-size: 22px !important;"> <span style="font-size: 22px !important;" class="tekst" id="cena3-{{$acc->id}}">{{$acc->price}}</span> $</p>
                            @else <p class="tekst mt-auto"  style="font-size: 22px !important;"> <span style="font-size: 22px !important;" class="tekst" id="cena3-{{$acc->id}}">{{$acc->price_eur}}</span> €</p>
                            @endif
                                <div class="def-number-input number-input safari_only mt-auto " style="margin-left: 13%;">
                                    <button style="cursor: url({{asset('images/cursor_2.png')}}), auto !important;" onclick="this.parentNode.querySelector('input[type=number]').stepDown(); myFunctionnnnn{{$acc->id}}(); mniejj{{$acc->id}}();" class="minus" id="minus"></button>
                                    <input class="quantity" min="1" max="{{ \App\AbcLeague\Repositories\AdminRepository::getAccountsCount($acc->id)}}" id="input-{{$acc->id}}" name="quantity" value="1" type="number" disabled>
                                    <button style="cursor: url({{asset('images/cursor_2.png')}}), auto !important;" onclick="this.parentNode.querySelector('input[type=number]').stepUp(); myFunctionnnnn{{$acc->id}}(); wiecejj{{$acc->id}}();"  class="plus"></button>

                                </div>
                                <p class="text-white">Buy 2 or more and save <b>10%</b></p>--}}
                                <style>
                                    .tooltiptext {

                                        width: 200px;
                                        background-color: orangered;
                                        color: #fff;
                                        text-align: center;
                                        padding: 1px 0;
                                        border-radius: 6px;
                                        margin-left: 5%;
                                        /* Position the tooltip text - see examples below! */

                                        z-index: 1;
                                    }
                                </style>
                                <p style="display: none" class="tooltiptext" id="demo-{{$acc->id}}"></p>
                            @if(\App\AbcLeague\Repositories\AdminRepository::getAccountsCount($acc->id) > 0)
                            <a style="cursor: url({{asset('images/cursor_red.png')}}), auto !important; border: none !important" data-target="#modal" class="btn btn-lg btn-block btn-outline-primary " data-toggle="modal" data-max="{{ \App\AbcLeague\Repositories\AdminRepository::getAccountsCount($acc->id)}}" data-desc="{{$acc->name }}" data-id="{{$acc->id}}" data-usd="{{$acc->price}}" data-euro="{{$acc->price_eur}}" data-name="{{$acc->slug}}" data-src="{{$acc->src}}">

                                @if(file_exists( public_path().'/images/Accounts/'.$acc->src.'.png' ))
                                <img class="align-self-end" src="../images/Accounts/{{$acc->src}}_buy.png"/>
                                @else
                                <img class="align-self-end" src="../images/Accounts/Platinum_buy.png"/>
                                @endif
                            </a>
                            @else
                            <p style="width: 120px; height: 46px; margin-left: 21%;" class="text-white-50  ">Out of stock</p>
                            @endif
                        </a>
                    </div>
                </div>
                    <script>
                        var mykk = document.getElementById("cena3-{{$acc->id}}").innerHTML;

                        function Round2(n, k)
                        {
                            var factor = Math.pow(10, 2);
                            return Math.round(n*factor)/factor;
                        }

                        function mniejj{{$acc->id}}(){

                            var test = parseFloat(document.getElementById("cena3-{{$acc->id}}").innerHTML);
                            var inpucik = parseInt(document.getElementById("input-{{$acc->id}}").value);
                            var total = parseInt(inpucik);

                            if(inpucik == 1){
                                document.getElementById("minus").disabled = true;
                                document.getElementById("cena3-{{$acc->id}}").innerHTML = mykk;
                            }else{
                                document.getElementById("minus").disabled = false;
                                document.getElementById("cena3-{{$acc->id}}").innerHTML = Round2(test - mykk);

                            }



                        }

                        function wiecejj{{$acc->id}}() {

                            const test = document.getElementById("cena3-{{$acc->id}}").innerHTML;
                            var inpucik = document.getElementById("input-{{$acc->id}}").value;

                            document.getElementById("cena3-{{$acc->id}}").innerHTML = Round2(mykk*inpucik);
                            if(inpucik == 1){
                                document.getElementById("minus").disabled = true;
                                document.getElementById("cena3-{{$acc->id}}").innerHTML = mykk;
                            }else {
                                document.getElementById("minus").disabled = false;
                            }
                        }
                        function myFunctionnnnn{{$acc->id}}() {





                            var x, text;
                            var y = {{ \App\AbcLeague\Repositories\AdminRepository::getAccountsCount($acc->id)}};
                            // Get the value of the input field with id="numb"
                            x = document.getElementById("input-{{$acc->id}}").value;
                            var wynik = (y-1);
                            // If x is Not a Number or less than one or greater than 10
                            if (isNaN(x) || x < 1 || x > wynik) {
                                text = "It's max value, more accounts are unavalible";
                                document.getElementById("demo-{{$acc->id}}").style.display = "block";
                                document.getElementById("demo-{{$acc->id}}").innerHTML = text;
                            } else {
                                text = "Input OK";
                                document.getElementById("demo-{{$acc->id}}").style.display = "none";
                            }

                        }
                    </script>
                @endforeach
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true"   >
        <div class="modal-dialog modal-dialog-centered" role="document">


            <div class="modal-content"  ><br>

                <div class="modal-body text-center center-block" style="background-color: gray">


                        @if($acc->currency == 'USD')
                        <p style="color: black !important;" class="wtf"><span id="test" class="wtf span" style="font-family: Geomanist,sans-serif !important;font-style: italic; font-size: 52px;"></span><span class="wtf"> $</span> </p>
                        @else
                        <p style="color: black !important;" class="wtf"><span id="test" class="wtf span2" style="font-family: Geomanist,sans-serif !important;font-style: italic; font-size: 52px;"></span><span class="wtf">€</span></p>
                        @endif
                        <div class="form-check " style="cursor: url({{asset('images/cursor_2.png')}}), auto !important;">
                            <img src="../../images/karton.png" style="width: 6%; margin-top: -1%"/><span class="namee"></span> <span style="font-size: 16px;">(<span id="xD" style="font-size: 16px;"></span>x)</span>
{{--                            <div class="def-number-input number-input safari_only mt-auto center-block " style="margin-left: 37%; margin-bottom: 1% !important; margin-top: 3px;">--}}
{{--                                <button style="cursor: url({{asset('images/cursor_2.png')}}), auto !important; background-color: white !important" onclick="this.parentNode.querySelector('input[type=number]').stepDown(); mniej();" class="minus" id="minus"></button>--}}
{{--                                <input class="quantity" min="1" max="" id="max-quan" name="max-quan" value="1" type="number" style="background-color: white;" disabled>--}}
{{--                                <button style="cursor: url({{asset('images/cursor_2.png')}}), auto !important; background-color: white !important" onclick="this.parentNode.querySelector('input[type=number]').stepUp(); wiecej();"  class="plus"></button>--}}
{{--                            </div>--}}
{{--                            <p class="text-black-50">Buy 2 or more and save <b>10%</b></p>--}}
                            <div class="row">
                                <div class="col center-block">
                                    <img style="width: 30%" src="../../images/Icons/1_acc.png">
                                    <p class="text-black-50 mt-1  text-center ml-5" style="width: 80% !important;">Account details will be shown <b>instantly after payment</b></p>
                                </div>
                                <div class="col w-50 center-block" style="margin-left: 2%">
                                    <img style="width: 18%" src="../../images/Icons/2_acc.png">
                                    <p class="text-black-50 w-75 text-center " style="margin-left: 12%">Secure <b>SSL encrypted</b> payment process</p>
                                </div>
                            </div>
                           <form action="/create_order" method="POST">
                            <div class="md-form input-group mb-3 " style="cursor: url({{asset('images/cursor_2.png')}}), auto !important; width:50%; margin-left: 24%">
                                <input type="text" class="form-control" style="cursor: url({{asset('images/cursor_2.png')}}), auto !important; " name="email" id="email"
                                       placeholder="Enter your email..." aria-describedby="material-addon1"  onchange="this.setCustomValidity(validity.valueMissing ? 'Please, enter your email' : '');" pattern="[a-zA-Z0-9!#$%&amp;'*+\/=?^_`{|}~.-]+@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*"  title="Please input a valid email" required >
                            </div>



                        @csrf
                        <input type="hidden" name="name" value=""/>
                        @if($acc->currency == 'USD')
                            <input type="hidden" name="price" id="priceusd" value=""/>
                        @else
                            <input type="hidden" name="price"  id="priceeur" value=""/>
                        @endif
                        <input type="hidden" id="src" name="src" value="" />
                        <input type="hidden" id="description" name="description" value="" />
                        <input type="hidden" id="order_id" name="order_id" value="{{\App\AbcLeague\Repositories\PaymentsRepository::trash()}}" />
                        <input type="hidden" id="id" name="id" value="" />
                        <input type="hidden" id="quantity" name="quantity" value="" />
                        <input type="hidden" id="kupon" name="kupon" value=""/>
                        <input type="hidden" name="currency" value="{{$acc->currency}}"/>
                        <input type="hidden" name="warranty_check" id="warranty_check"  value=""/>

                        <div>

                            <button type="submit" name="type" value="stripe" style="background-color: white; width:34%" class="btn btn-outline-primary  mr-1 ml-1">
                                <img class=" w-100 mr-1 " style="padding: 0px;" src="../../images/Check/payment_cards.png" />
                            </button>
                            <button type="submit" name="type" value="paypal"  style="background-color: white; width: 25%" class="btn btn-outline-primary  mr-1 ml-1">
                                <img class=" w-100 mr-1 ml-1" src="../../images/Check/paypal.png" />
                            </button>
                        </div>

            </form>
                    <div class="md-form input-group input-group-sm mb-3 mt-1 text-center block-center ">
                        <div class="md-form input-group  " style="margin-left: 29%;">
                            <form lang="en">
                                @csrf
                                <p class="success" style="color: green !important; font-weight: bold !important; text-align: center"></p>
                                <p class="failed" style="color: red !important; font-weight: bold !important; text-align: center"></p>
                                <input type="text" id="coupon_text" style="cursor: url({{asset('images/cursor_2.png')}}), auto !important;" class="form-control block-center" placeholder="Coupon">
                                <button id="checkCoupon" onclick="japierdole()" style="cursor: url({{asset('images/cursor_2.png')}}), auto !important;" class="btn btn-dark btn-rounded block-center">Apply</button>

                            </form>
                        </div>
                    </div>
                    <img class=""  style="width:35%; " src="{{asset('images/white-logob.png')}}"/>
                            <p class="text-black-50">By clicking the button, you agree to our <b><a href="/tos" style="color: black !important">Terms & Conditions</a></b></p>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function myFunction() {
        var checkBox = document.getElementById("warranty");
        var text = parseFloat(document.getElementById('test').innerText);
        var przelicz = parseFloat(text * 1.25);
        var cofnij = parseFloat(text / 1.25);

        if (checkBox.checked == true){
            document.getElementById("warranty_check").value = 'Yes';
            @if($acc->currency == 'USD')
            document.getElementById("priceusd").value = Round(przelicz);
            @else
            document.getElementById("priceeur").value = Round(przelicz);
            @endif
            document.getElementById('test').innerHTML = Round(przelicz) + ' ';
        }

        if (checkBox.checked == false){
            document.getElementById("warranty_check").value = 'No'
            @if($acc->currency == 'USD')
            document.getElementById("priceusd").value = Round(cofnij);
            @else
            document.getElementById("priceeur").value = Round(cofnij);
            @endif;
            document.getElementById('test').innerHTML = Round(cofnij) + ' ';
        }

        if (checkBox.checked == true){

            document.getElementById("priceusd").value = Round(przelicz);
        }

        if (checkBox.checked == false){
            document.getElementById("priceusd").value = Round(cofnij);
        }


    }
    $('#modal').on('show.bs.modal', function (event) {
        document.getElementById("xD").innerHTML = 1;
      // test_wiecejj = parseFloat(document.getElementById("test").innerHTML);
        test_wiecejj = $(event.relatedTarget).data('usd')
    });
    function Round2(n, k)
    {
        var factor = Math.pow(10, 2);
        return Math.round(n*factor)/factor;
    }

    function mniej(){

        var obecna = document.getElementById("test").innerHTML;
        // alert(obecna);
        var inpucik = parseInt(document.getElementById("max-quan").value);
       document.getElementById("xD").innerHTML = inpucik;
        var total = parseInt(test);

        if(inpucik == 1){
            // document.getElementById("minus").disabled = true;
            document.getElementById("test").innerHTML = test_wiecejj;
            document.getElementById("quantity").value = inpucik;
            document.getElementById("priceusd").value = test_wiecejj * inpucik;
            document.getElementById("priceeur").value = test_wiecejj * inpucik;
        }else{
            // document.getElementById("minus").disabled = false;
            document.getElementById("test").innerHTML = Round2(obecna - test_wiecejj);
            document.getElementById("quantity").value = inpucik;
            document.getElementById("priceusd").value = test_wiecejj * inpucik;
            document.getElementById("priceeur").value = test_wiecejj * inpucik;
        }
    }

    function wiecej() {

        var inpucik = parseFloat(document.getElementById("max-quan").value);
        var total = parseFloat(test);
        document.getElementById("xD").innerHTML = inpucik;
        document.getElementById("test").innerHTML = Round2(test_wiecejj*inpucik);
         // test_wiecejj = parseFloat(test_wiecejj/inpucik);
        document.getElementById("quantity").value = inpucik;
        document.getElementById("priceusd").value = test_wiecejj * inpucik ;
        document.getElementById("priceeur").value = test_wiecejj * inpucik;
        if(inpucik == 1){

            document.getElementById("test").innerHTML = test_wiecej;


        }else {
            test_wiecejj = parseFloat(ha/inpucik);
            // document.getElementById("minus").disabled = false;
        }
    }
    function myFunctionnn() {
        var x, text;
        var y = document.getElementById("max-quan").getAttribute('max');
        // Get the value of the input field with id="numb"
        x = document.getElementById("input-max-quan").value;
        var wynik = (y-1);
        // If x is Not a Number or less than one or greater than 10
        if (isNaN(x) || x < 1 || x > wynik) {
            text = "It's max value, more accounts are unavalible";
            document.getElementById("demo-{{$acc->id}}").style.display = "block";
            document.getElementById("demo-{{$acc->id}}").innerHTML = text;
        } else {
            text = "Input OK";
            document.getElementById("demo-{{$acc->id}}").style.display = "none";
        }
    }



</script>
<script>
    $(document).ready(function(){
        $('input:checkbox').prop('checked', false);
    });
    $('#modal').on('show.bs.modal', function (event) {
        $('input:checkbox').prop('checked', false);

        $("#max-quan").val(1);

        jQuery('.success').hide();
        jQuery('.failed').hide();
        document.getElementById("checkCoupon").disabled = false;

        var ID = $(event.relatedTarget).data('id')
        var input = 'input-' + ID;

        let quantity = 1;
        let usd = $(event.relatedTarget).data('usd')
        let desc = $(event.relatedTarget).data('desc')
        let src = $(event.relatedTarget).data('src')
        let euro = $(event.relatedTarget).data('euro')
        let name = $(event.relatedTarget).data('name')
        let max = $(event.relatedTarget).data('max')
        let euro_finish = parseFloat(quantity * euro);
        let usd_finish = parseFloat(quantity * usd);

        let pyk_usd = Round(usd_finish);
        let pyk_euro = Round(euro_finish);

        $(this).find('.modal-body .span').html(pyk_usd + ' ' )
        $(this).find('.modal-body .span2').html(pyk_euro + ' ' )
        $(this).find('.modal-body input[name=\'name\']').val(name)
        $(this).find('.modal-body .namee').html(name)
        $(this).find('.modal-body input[id=\'priceusd\']').val(pyk_usd)
        $(this).find('.modal-body input[id=\'description\']').val(desc)
        $(this).find('.modal-body input[id=\'priceeur\']').val(pyk_euro)
        $(this).find('.modal-body input[id=\'max-quan\']').attr("max",max);
        $(this).find('.modal-body input[id=\'src\']').val(src)
        $(this).find('.modal-body input[id=\'quantity\']').val(quantity)
        $(this).find('.modal-body input[id=\'id\']').val(ID)
    });
</script>

<script>

    document.getElementById("tos").setCustomValidity("Please fill this checkbox first.");
    var inputttt = document.getElementById("email");
    inputttt.oninvalid = function(event) {
        event.target.setCustomValidity('Please input a valid email');
    }

    function Round(n, k)
    {
        var factor = Math.pow(10, 2);
        return Math.round(n*factor)/factor;
    }

    // jQuery(document).ready(function(){
        function japierdole() {
            $("#checkCoupon").click(function (e) {
                e.preventDefault();
                jQuery.ajaxSetup({
                    headers: {
                        "_token": "{{ csrf_token() }}"
                    }
                });
                $.ajax({
                    url: "{{ url('/coupon/check') }}",
                    method: 'post',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        coupon_test: $("#coupon_text").val(),
                    },
                    success: function (result) {
                        if (result.msg !== 'true') {
                            jQuery('.success').hide();
                            jQuery('.failed').show();
                            jQuery('.failed').html(result.failed);
                            document.getElementById("kupon").value = 'No';

                        } else {
                            document.getElementById("kupon").value = 'Yes';
                            document.getElementById("checkCoupon").disabled = true;
                            jQuery('.failed').hide();
                            jQuery('.success').show();
                            jQuery('.success').html('Coupon on ' + result.discount + '% discount applied');
                            var test = parseFloat(document.getElementById("test").innerText);
                            var pykk = test - (test * (result.discount / 100));
                            document.getElementById("test").innerHTML = Round(pykk);
                            document.getElementById("priceusd").value = Round(pykk);
                            document.getElementById("priceeur").value = Round(pykk);
                            document.getElementById("checkCoupon").disabled = true;
                        }
                    }
                });
            });
        }
    // });

    $(document).ready(function(){
        $("#modal").on('hide.bs.modal', function(){
            $('#warranty').prop('checked', false);
            $("#max-quan").val(1);

            jQuery('.success').hide();
            jQuery('.failed').hide();
            document.getElementById("checkCoupon").disabled = false;
        });
    });


   </script>
</div>
</div>

</body>

<footer>
    <div class="row">
        <div class="col-md">
            <div class="text-center mt-3" style="color: black !important;"> <b>FAQ</b> </div>

            <div class="clearfix"></div>

            <div class="accordion" id="accordionExample" style="margin-left: 12%;cursor: url({{asset('images/cursor_2.png')}}), auto !important;">
                <div class="card" style="background-color: #FFFFFF !important; border: none">
                    <div class="card-header" style="background-color: #FFFFFF !important;" id="headingOne">
                        <h2 class="mb-0">
                            <button class="btn btn-link" style="background-color: #FFFFFF !important;cursor: url({{asset('images/cursor_2.png')}}), auto !important; color: black !important" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                <b>1. Can I change the account details?</b>
                            </button>
                        </h2>
                    </div>

                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                        <div class="card-body">
                            Yes! All of our LoL accounts come with login details so you can log in to your account and change anything you like.
                        </div>
                    </div>
                </div>
                <div class="card" style="background-color: #FFFFFF !important; border: none">
                    <div class="card-header" style="background-color: #FFFFFF !important;" id="headingTwo">
                        <h2 class="mb-0">
                            <button class="btn btn-link collapsed" style="background-color: #FFFFFF !important;cursor: url(images/cursor_2.png), auto !important;color: black !important" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <b>2. I have not received an account</b>
                            </button>
                        </h2>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                        <div class="card-body">
                            Make sure that you check your spam mail folder. If it’s still not there please contact us.                        </div>
                    </div>
                </div>
                <div class="card" style="background-color: #FFFFFF !important; border: none">
                    <div class="card-header" style="background-color: #FFFFFF !important;" id="headingThree">
                        <h2 class="mb-0">
                            <button class="btn btn-link collapsed" style="background-color: #FFFFFF !important;cursor: url(images/cursor_2.png), auto !important;color: black !important" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <b>3. What name will my account have?</b>
                            </button>
                        </h2>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                        <div class="card-body">
                            The name of the account is randomized, although they sound legit and catchy. If you are not satisfied with your name you can always buy a namechange for 13900 BE in the shop.                            </div>
                    </div>
                </div>
                <div class="card " style="background-color: #FFFFFF !important; border: none">
                    <div class="card-header" style="background-color: #FFFFFF !important;" id="headingFour">
                        <h2 class="mb-0">
                            <button class="btn btn-link collapsed" style="background-color: #FFFFFF !important;cursor: url(images/cursor_2.png), auto !important;color: black !important" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <b>4. My account got banned</b>
                            </button>
                        </h2>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                        <div class="card-body">
                            If your account gets banned within 30 days (90 days if you purchased extended warranty) of purchase message us and we will send you a new one. We do not refund money for banned accounts!                          </div>
                    </div>
                </div>
                <div class="card" style="background-color: #FFFFFF !important; border: none">
                    <div class="card-header" style="background-color: #FFFFFF !important;" id="headingFive">
                        <h2 class="mb-0">
                            <button class="btn btn-link collapsed" style="background-color: #FFFFFF !important;cursor: url({{asset('images/cursor_2.png')}}), auto !important; color: black !important" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                <b>5. How long does it take to deliver an account?</b>
                            </button>
                        </h2>
                    </div>
                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
                        <div class="card-body">
                            Account will be delivered to you instantly after purchase.                         </div>
                    </div>
                </div>
            </div>
            </br>
            <script>
                var $myGroup = $('#accordion');
                $myGroup.on('show.bs.collapse','.collapse', function() {
                    $myGroup.find('.collapse.show').collapse('hide');
                });
            </script>

            <div class="ukryte"><p style="color: black !important;"><b></b></p></div>
            </ul>
            </br>
            </ul>
        </div>
        <div class="col-sm">
            <p class="text-center mt-3" style="color: black !important;"> <b>Contact with</b> </p>
            <ul class="faq-list">

                <li style="margin-bottom: 2%;cursor: url(images/cursor_2.png), auto !important;"><img style="width:14%" src="{{asset('images/email.png')}}"/> contact@abc-league.com </li>
                <li style="cursor: url(images/cursor_2.png), auto !important;"><img style="width:14%" src="{{asset('images/discord2.png')}}"/> ABC-league Support#3440 </li>
                <li style="cursor: url(images/cursor_2.png), auto !important; color: black !important"><img style="width:15%" src="{{asset('images/skype.png')}}"/> <a style="color: black !important" href="skype:live:.cid.94f158f84e0cd2dd?chat"> ABC League</a> </li>

            </ul>
        </div>
        <div class="col-sm"  style="margin-top: -2%; margin-left: 3%;">
            <img class="w-50 mb-5" style="margin-left: 3%;" src="{{asset('images/footer.png')}}"/></br>
            <div style="margin-top: -25%; margin-left: 3%;">

                <img class="w-25 " src="{{asset('images/Payments/Psc.png')}}"/>
                <img class="w-25 " src="{{asset('images/Payments/Paypal.png')}}"/></br>
                <img class="w-50 " src="{{asset('images/Payments/all2.png')}}"/></br>

            </div>
        </div>
    </div>
    </div>
    <hr style="height: 2px; color: black; background-color: black; margin-bottom: 0px !important; margin-top: 0px !important;">

    <script>
        $(document).ready(function() {

            $(".toggle-accordion").on("click", function() {
                var accordionId = $(this).attr("accordion-id"),
                    numPanelOpen = $(accordionId + ' .collapse.in').length;

                $(this).toggleClass("active");

                if (numPanelOpen == 0) {
                    openAllPanels(accordionId);
                } else {
                    closeAllPanels(accordionId);
                }
            })

            openAllPanels = function(aId) {
                console.log("setAllPanelOpen");
                $(aId + ' .panel-collapse:not(".in")').collapse('show');
            }
            closeAllPanels = function(aId) {
                console.log("setAllPanelclose");
                $(aId + ' .panel-collapse.in').collapse('hide');
            }

        });
    </script>

    <div style="height: 20px;  background-color: white !important;">
        <p style="margin-bottom: 1px; margin-left: 10%; color: black !important; float:left !important">Copyright 2020 | <b>ABC-LEAGUE</b></p>
    </div>
</footer>

<script src="{{asset('js/main.js')}}"></script>

<script src="{{asset('js/flickity.pkgd.min.js')}}"></script>
<script>
    $(document).ready(function(){
        setTimeout(function () {
            $("#cookieConsent").fadeIn(200);
        }, 4000);
        $("#closeCookieConsent, .cookieConsentOK").click(function() {
            $("#cookieConsent").fadeOut(200);
        });
    });
</script>
<script>
    $(document).ready(function(){

        $("#closeCookieConsent2, .cookieConsentOK2").click(function() {
            $("#cookieConsent2").fadeOut(200);
        });
    });
</script>
{{--<script>--}}
{{--        document.addEventListener("contextmenu", function(e){--}}
{{--            e.preventDefault();--}}
{{--        }, false);--}}
{{--    </script>--}}
</html>





