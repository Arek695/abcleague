@extends('layouts.app')
@desktop
@section('content')
<style>
    .filtered {
        -webkit-filter: grayscale(100%);
        filter: grayscale(100%);
    }
</style>
    <div class="flex-center position-ref full-height">

<style>
    .main{
         background-image: url("{{asset('images/bg_tos.png')}}") !important;

     }
    body{
         background-image: url("{{asset('images/bg_tos.png')}}") !important;

     }
</style>
        <!-- Page Content -->
        <div class="container">


            <!-- Page Features -->
            <div class="row text-center mt-5">
                @foreach ($regions as $region)
                    <div class="col-lg-4 col-md-6 mb-4">
                        <div class="w-75">
                            @if($region->avalible == 1)
                            <a href="/accounts/{{$region->name}}">
                                <img class="card-img-top {{$region->style}}" style="text-align: left; " src="../images/Regions/{{$region->image}}" alt="">
                                <img class="w-100"src="../images/Review.png"/>
                            </a>
                            @elseif($region->avalible == 0)
                                <a >
                                    <img class="card-img-top {{$region->style}}" style="text-align: left; " src="../images/Regions/{{$region->image}}" alt="">
                                    <img class="w-100" src="../images/Review.png"/>
                                </a>
                            @endif

                        </div>
                        <p class="tekst">{{$region->full_name}}</p>
                    </div>

                @endforeach

            </div>
            <!-- /.row -->


        </div>
        <!-- /.container -->

    </div>
    <div id="preloder">
        <div class="loader"></div>
    </div>
@endsection
@elsedesktop

@section('content')

    <div class="flex-center position-ref full-height">


        <!-- Page Content -->
        <div class="container" style="margin-left: 13%;">


            <!-- Page Features -->
            <div class="row text-center mt-5">
                @foreach ($regions as $region)
                    <div class="col-lg-4 col-md-6 mb-4">
                        <div class="w-75">
                            <a href="/accounts/{{$region->id}}">
                                <img class="card-img-top" style="text-align: left; " src="../images/Regions/{{$region->image}}" alt="">
                                <img class="w-100"src="../images/Review.png"/>
                            </a>
                        </div>
                        <p class="tekst">{{$region->full_name}}</p>
                    </div>

                @endforeach

            </div>
            <!-- /.row -->


        </div>
        <!-- /.container -->

    </div>
    <div id="preloder">
        <div class="loader"></div>
    </div>
@endsection
@enddesktop

