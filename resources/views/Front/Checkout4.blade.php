<html lang="pl">

<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="stylesheet" href="{{asset('vendor/fontawesome-free/css/all.css')}}">
    <script src="{{ asset('js/app.js') }}" defer></script>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="robots" content="noindex">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>ABCLeague - Payment</title>
    <link href="https://js.stripe.com/v3/fingerprinted/css/checkout-b3cdba85de873de05cb1754c19d5b646.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>

</head>

<body>
<noscript>You need to enable JavaScript to run this app.</noscript>
<div id="root">
    <div class="App-Container is-noBackground App-Container is-noBackground flex-container justify-content-center">
        <div class="App-Background" style="background-color: rgb(255, 255, 255);"></div>
        <div class="App App--singleItem" style="margin-top: 113px;">
            <div class="App-Overview">
                <header class="Header" style="background-color: rgb(255, 255, 255);">
                    <div class="Header-Content Header-Content flex-container justify-content-space-between align-items-stretch">
                        <div class="Header-Business Header-Business flex-item width-auto">
                            <a class="Link Header-BusinessLink Link--primary" href="https://abc-league.com" target="_self" aria-label="Poprzednia strona" title="Go back">
                                <div style="position: relative;">
                                    <div class="flex-container align-items-center">
                                        <svg class="InlineSVG Icon Header-BusinessLink-arrow mr2 Icon--sm" focusable="false" width="12" height="12" viewBox="0 0 16 16">
                                            <path d="M3.417 7H15a1 1 0 0 1 0 2H3.417l4.591 4.591a1 1 0 0 1-1.415 1.416l-6.3-6.3a1 1 0 0 1 0-1.414l6.3-6.3A1 1 0 0 1 8.008 2.41z" fill-rule="evenodd"></path>
                                        </svg>
                                        <div class="flex-container align-items-center">
                                            <div class="HeaderImage--icon HeaderImage--icon flex-item width-fixed HeaderImage--icon flex-container justify-content-center align-items-center"><img alt="Stripe Press" src="https://stripe-camo.global.ssl.fastly.net/5527aa500b7ed436f3fdb052938641d2ddb454f8/68747470733a2f2f66696c65732e7374726970652e636f6d2f66696c65732f665f746573745f6c31766b6e756647586c7a614e35616a6e35334f4c386f66"></div>
                                            <div class="Header-BusinessLink-name-wrapper"><span class="Header-BusinessLink-label Text Text-color--gray800 Text-fontSize--14 Text-fontWeight--500"><span>Go back</span></span>
                                                <h1 class="Header-BusinessLink-name Text Text-color--gray800 Text-fontSize--14 Text-fontWeight--500 Text--truncate">ABCLeague</h1></div>
                                        </div>
                                        <div class="Tag mx2"><span class="Text Text-color--orange Text-fontSize--11 Text-fontWeight--700 Text-transform--uppercase">Live</span></div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </header>
                <div class="ProductSummary mt6">
                    <div class="ProductSummary-ProductImageContainer">
                        <div class="ProductImage-container"><img src="../images/Accounts/test/{{$src}}.png" alt="Product" class="ProductImage-image"></div>
                    </div>
                    <div class="ProductSummary-Info"><span class="Text Text-color--gray500 Text-fontSize--16 Text-fontWeight--500">{{$name}}</span><span class="ProductSummary-TotalAmount Text Text-color--gray800 Text-fontWeight--600 Text--tabularNumbers" id="ProductSummary-TotalAmount"><span>{{$price}}&nbsp;{{$currency}}</span></span></div>
                </div>
            </div>
            <div class="App-Payment">
                <div class="PaymentRequestOrHeader" style="height: 124px;">
                    <div class="PaymentHeaderContainer" style="opacity: 0; display: none;">
                        <div class="PaymentHeader">
                            <div class="Text Text-color--default Text-fontSize--20 Text-fontWeight--500"><span>Płatność kartą</span></div>
                        </div>
                    </div>

                </div>
                <form role="form" action="/pay/stripe/{{$order_id}}" method="post" class="require-validation"
                      data-cc-on-file="false"
                      data-stripe-publishable-key="pk_live_KpPaLk4CKLhEUlTA5itof0Ub00JyF4MuSL"
                      id="payment-form">
                    @csrf
                    @method('POST')
                    <input type="hidden" name="name" id="name" value="{{$name}}"/>
                    <input type="hidden" name="price" id="price" value="{{$price}}"/>
                    <input type="hidden" name="email" id="email" value="{{$email}}"/>
                    <input type="hidden" name="currency" id="currency" value="{{$currency}}"/>
                    <input type="hidden" name="order_id" id="order_id" value="{{$order_id}}"/>
                    <input type="hidden" name="quantity" id="quantity" value="{{$quantity}}"/>
                    </br>
                    <div class='form-row row'>
                        <div class='col-xs-12 form-group  required ' style="border: none !important; background-color: transparent !important; ">
                            <label style="background-color: transparent !important; " class='Text Text-color--black Text-fontSize--13 Text-fontWeight--500'>Name on card</label> <input
                                autocomplete='off' placeholder="John Smith" class='form-control card-name' size='40' name="lastsur"
                                type='text' style="width: 100% !important">
                        </div>
                    </div>
                    <style>
                        .border-button {
                            outline: solid 1px #000000;
                            transition: outline 0.1s linear;
                            margin: 0.5em; /* Increased margin since the outline expands outside the element */
                        }

                        .border-button:hover { outline-width: 1px; }
                        ::-webkit-input-placeholder {
                            text-align: center;
                        }

                        :-moz-placeholder { /* Firefox 18- */
                            text-align: center;
                        }

                        ::-moz-placeholder {  /* Firefox 19+ */
                            text-align: center;
                        }

                        :-ms-input-placeholder {
                            text-align: center;
                        }
                    </style>
                    <div class='form-row row'>
                        <div class='col-xs-12 form-group  required ' style="border: none !important; background-color: transparent !important; ">
                            <label style="background-color: transparent !important; " class='Text Text-color--black Text-fontSize--13 Text-fontWeight--500'>Card Number</label> <input
                                autocomplete='off' placeholder="1234 1234 1234 1234" class='form-control card-number' size='40'
                                type='text' style="width: 100% !important;">
                        </div>
                    </div>

                    <div class='form-row row'>
                        <div class='col-xs-12 col-md-4 form-group cvc required'>
                            <label class='Text Text-color--black Text-fontSize--13 Text-fontWeight--500'>CVC (three digits)</label> <input autocomplete='off'
                                                                                                                                           class='form-control card-cvc' placeholder='ex. 311' size='4'
                                                                                                                                           type='text' >
                        </div>
                        <div class='col-xs-12 col-md-4 form-group expiration required'>
                            <label class='Text Text-color--black Text-fontSize--13 Text-fontWeight--500'>Expiration Month</label> <input
                                class='form-control card-expiry-month' placeholder='MM' size='2'
                                type='text'>
                        </div>
                        <div class='col-xs-12 col-md-4 form-group expiration required'>
                            <label class='Text Text-color--black Text-fontSize--13 Text-fontWeight--500'>Expiration Year</label> <input
                                class='form-control card-expiry-year' placeholder='YYYY' size='4'
                                type='text' style="width: 90% !important;">
                        </div>
                    </div>
                    <center><button class="btn btn-dark btn-lg btn-block" style="width: 90%" type="submit">Pay Now {{$price}} {{$currency}}</button></center>

                    <div class='form-row row'>
                        <div class='col-md-12 error form-group hide'>
                            <div class='alert-danger alert'>Please correct the errors and try
                                again.</div>
                        </div>



                    </div>

            </div>
        </div>
        </div>
        </form>
            </div>

        </div>
    </div>
</div>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>

<script type="text/javascript">
    $(function() {
        var $form         = $(".require-validation");
        $('form.require-validation').bind('submit', function(e) {
            var $form         = $(".require-validation"),
                inputSelector = ['input[type=email]', 'input[type=password]',
                    'input[type=text]', 'input[type=file]',
                    'textarea'].join(', '),
                $inputs       = $form.find('.required').find(inputSelector),
                $errorMessage = $form.find('div.error'),
                valid         = true;
            $errorMessage.addClass('hide');

            $('.has-error').removeClass('has-error');
            $inputs.each(function(i, el) {
                var $input = $(el);
                if ($input.val() === '') {
                    $input.parent().addClass('has-error');
                    $errorMessage.removeClass('hide');
                    e.preventDefault();
                }
            });

            if (!$form.data('cc-on-file')) {
                e.preventDefault();
                Stripe.setPublishableKey($form.data('stripe-publishable-key'));
                Stripe.createToken({
                    number: $('.card-number').val(),
                    cvc: $('.card-cvc').val(),
                    exp_month: $('.card-expiry-month').val(),
                    exp_year: $('.card-expiry-year').val()
                }, stripeResponseHandler);
            }

        });

        function stripeResponseHandler(status, response) {
            if (response.error) {
                $('.error')
                    .removeClass('hide')
                    .find('.alert')
                    .text(response.error.message);
            } else {
                // token contains id, last4, and card type
                var token = response['id'];
                // insert the token into the form so it gets submitted to the server
                $form.find('input[type=text]').empty();
                $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
                $form.get(0).submit();
            }
        }

    });
</script>
</body>

</html>
