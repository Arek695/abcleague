@desktop
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" style="overscroll-behavior: none;">
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-WPHJN3V');</script>
    <!-- End Google Tag Manager -->

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WPHJN3V"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <script src="//code.tidio.co/orgswuezax7hliotdaqjm7yw2bc0wswn.js" async></script>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        a:hover {
            cursor: url(images/cursor_2.png), auto !important;
        }
        body{
            cursor: url(images/asd.png), auto !important;

        }
        html{
            cursor: url(images/asd.png), auto !important;

        }

        .frame-content{
            cursor: url(images/asd.png), auto !important;
        }

        .widgetLabel{
            cursor: url(images/asd.png), auto !important;
        }
        .moveFromRightLabel-enter-done
        {
            cursor: url(images/asd.png), auto !important;
        }
        #button{
            cursor: url(images/asd.png), auto !important;
        }
        span{
            cursor: url(images/asd.png), auto !important;
        }

    </style>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>ABC League</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>


    <link rel="stylesheet" href="{{asset('vendor/fontawesome-free/css/all.css')}}">
    <script src="{{ asset('js/app.js') }}" defer></script>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

<!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/flickity.css')}}" media="screen">
<style>
    .navbar-light .navbar-nav .nav-link {
        font-size: 18px !important;
        color: white;
    }
</style>
    <nav class="navbar navbar-expand-md navbar-light bg-blue shadow-sm">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                <img class="rounded logo" style="width: 30%!important" src="{{asset('images/white-logo.png')}}"/>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    <li class="nav-item ">
                        <a class="nav-link" href="/accounts">Accounts</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/tos"><b>TOS</b></a>
                    </li>

                    <li class="nav-item">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="{{$start}}">
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <form method="POST" action="{{route('currency')}}" class="cyk" >@csrf @method('POST')<input type="hidden" class="cyk" name="currency" value="USD"><input type="hidden" class="cyk" name="name" value="{{asset('images/dolar_nowy.png')}}">
                                <button type="submit" class="dropdown-item cyk"  style="cursor: url(images/cursor_2.png), auto !important;;" href="#"><img class="cyk" src="{{asset('images/dolar_nowy.png')}}" />Dollar</button></form>
                            <form method="POST" action="{{route('currency')}}" class="cyk">@csrf @method('POST')<input type="hidden" class="cyk" name="currency" value="EUR"><input type="hidden" class="cyk" name="name" value="{{asset('images/euro_nowe.png')}}">
                                <button type="submit" class="dropdown-item cyk"  style="cursor: url(images/cursor_2.png), auto !important;;" href="#"><img class="cyk" src="{{asset('images/euro_nowe.png')}}" />Euro</button></form>


                        </div>
                    </li>
                </ul>
            </div>
        </div>

    </nav>
</head>
<body>
<div class="main">
@yield('content')
</div>
</div>

</body>

<footer>
    <div class="row">
        <div class="col-md">
            <div class="text-center mt-3" style="color: black !important;"> <b>FAQ</b> </div>
{{--            <ul class="faq-list">--}}
{{--                <li id="a">1. Can I change the account details?.</li>--}}

{{--                <div id="a1" class="ukryte"><p style="color: black !important;"><b>-Yes! All of our LoL accounts come with login details so you can log in to your account and change anything you like.</b></p></p>--}}
{{--            </ul> <ul class="faq-list">--}}
{{--                <li id="b">2. I have not received an account after purchase.</li>--}}

{{--                <div id="b1" class="ukryte"><p style="color: black !important;"><b>-Make sure that you check your spam mail folder. If it’s still not there please contact us.</b></p></p>--}}
{{--            </ul><ul class="faq-list">--}}
{{--                <li id="c">3. What name will my account have?</li>--}}

{{--                <div id="c1" class="ukryte"><p style="color: black !important;"><b>-Make sure that you check your spam mail folder. If it’s still not there please contact us.</b></p></p>--}}
{{--            </ul><ul class="faq-list">--}}
{{--                <li id="d">4. My account got banned.</li>--}}

{{--                <div id="d1" class="ukryte"><p style="color: black !important;"><b>-If your account gets banned within 30 days (90 days if you purchased extended warranty) of purchase message us and we will send you a new one. We do not refund money for banned accounts!</b></p></p>--}}
{{--            </ul>--}}
{{--            <ul class="faq-list">--}}
{{--                <li id="e">5. How long does it take to deliver an account?</li>--}}
{{--                <div id="e1" class="ukryte"><p style="color: black !important;"><b>-Account will be delivered to you instantly after purchase.</b></p></p>--}}

{{--            </ul>--}}

            <style>
                .btn-link{
                color: black;

                }
                .accordion > .card .card-header {
                    margin-bottom: -17px !important;
                }
            </style>
            <div class="clearfix"></div>

            <div class="accordion" id="accordionExample" style="margin-left: 12%;">
                <div class="card" style="background-color: #FFFFFF !important; border: none">
                    <div class="card-header" style="background-color: #FFFFFF !important;" id="headingOne">
                        <h2 class="mb-0">
                            <button class="btn btn-link" style="background-color: #FFFFFF !important;cursor: url(images/cursor_2.png), auto !important;" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                <b>1. Can I change the account details?</b>
                            </button>
                        </h2>
                    </div>

                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                        <div class="card-body">
                            Yes! All of our LoL accounts come with login details so you can log in to your account and change anything you like.
                        </div>
                    </div>
                </div>
                <div class="card" style="background-color: #FFFFFF !important; border: none">
                    <div class="card-header" style="background-color: #FFFFFF !important;" id="headingTwo">
                        <h2 class="mb-0">
                            <button class="btn btn-link collapsed" style="background-color: #FFFFFF !important;cursor: url(images/cursor_2.png), auto !important;" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <b>2. I have not received an account</b>
                            </button>
                        </h2>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                        <div class="card-body">
                            Make sure that you check your spam mail folder. If it’s still not there please contact us.                        </div>
                    </div>
                </div>
                <div class="card" style="background-color: #FFFFFF !important; border: none">
                    <div class="card-header" style="background-color: #FFFFFF !important;" id="headingThree">
                        <h2 class="mb-0">
                            <button class="btn btn-link collapsed" style="background-color: #FFFFFF !important;cursor: url(images/cursor_2.png), auto !important;" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <b>3. What name will my account have?</b>
                            </button>
                        </h2>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                        <div class="card-body">
                            The name of the account is randomized, although they sound legit and catchy. If you are not satisfied with your name you can always buy a namechange for 13900 BE in the shop.                            </div>
                    </div>
                </div>
                <div class="card " style="background-color: #FFFFFF !important; border: none">
                    <div class="card-header" style="background-color: #FFFFFF !important;" id="headingFour">
                        <h2 class="mb-0">
                            <button class="btn btn-link collapsed" style="background-color: #FFFFFF !important;cursor: url(images/cursor_2.png), auto !important;" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <b>4. My account got banned</b>
                            </button>
                        </h2>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                        <div class="card-body">
                            If your account gets banned within 30 days (90 days if you purchased extended warranty) of purchase message us and we will send you a new one. We do not refund money for banned accounts!                          </div>
                    </div>
                </div>
                <div class="card" style="background-color: #FFFFFF !important; border: none">
                    <div class="card-header" style="background-color: #FFFFFF !important;" id="headingFive">
                        <h2 class="mb-0">
                            <button class="btn btn-link collapsed" style="background-color: #FFFFFF !important;cursor: url({{asset('images/cursor_2.png')}}), auto !important;" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                <b>5. How long does it take to deliver an account?</b>
                            </button>
                        </h2>
                    </div>
                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
                        <div class="card-body">
                            Account will be delivered to you instantly after purchase.                         </div>
                    </div>
                </div>
            </div>
            </br>
                <script>
                    var $myGroup = $('#accordion');
                    $myGroup.on('show.bs.collapse','.collapse', function() {
                        $myGroup.find('.collapse.show').collapse('hide');
                    });
                </script>

            <div class="ukryte"><p style="color: black !important;"><b></b></p></div>
            </ul>
            </br>
            </ul>
        </div>
        <div class="col-sm">
            <p class="text-center mt-3" style="color: black !important;"> <b>Contact</b> </p>
            <ul class="faq-list">

                <li style="margin-bottom: 2%;cursor: url(images/cursor_2.png), auto !important;"><img style="width:14%" src="{{asset('images/email.png')}}"/> contact@abc-league.com </li>
                <li style="cursor: url(images/cursor_2.png), auto !important;"><img style="width:14%" src="{{asset('images/discord2.png')}}"/> Luka#5403 </li>
                <li style="cursor: url(images/cursor_2.png), auto !important;"><img style="width:15%" src="{{asset('images/skype.png')}}"/> <a href="skype:live:.cid.94f158f84e0cd2dd?chat"> ABC League</a> </li>

            </ul>
        </div>
        <div class="col-sm"  style="margin-top: -2%; margin-left: 3%;">
            <img class="w-50 mb-5" style="margin-left: 3%;" src="{{asset('images/footer.png')}}"/></br>
            <div style="margin-top: -25%; margin-left: 3%;">

                <img class="w-25 " src="{{asset('images/Payments/Psc.png')}}"/>
                <img class="w-25 " src="{{asset('images/Payments/Paypal.png')}}"/></br>
                <img class="w-50 " src="{{asset('images/Payments/all2.png')}}"/></br>

            </div>
        </div>
    </div>
    </div>
    <hr style="height: 2px; color: black; background-color: black; margin-bottom: 0px !important; margin-top: 0px !important;">

    <script>
        $(document).ready(function() {

            $(".toggle-accordion").on("click", function() {
                var accordionId = $(this).attr("accordion-id"),
                    numPanelOpen = $(accordionId + ' .collapse.in').length;

                $(this).toggleClass("active");

                if (numPanelOpen == 0) {
                    openAllPanels(accordionId);
                } else {
                    closeAllPanels(accordionId);
                }
            })

            openAllPanels = function(aId) {
                console.log("setAllPanelOpen");
                $(aId + ' .panel-collapse:not(".in")').collapse('show');
            }
            closeAllPanels = function(aId) {
                console.log("setAllPanelclose");
                $(aId + ' .panel-collapse.in').collapse('hide');
            }

        });
    </script>
<style>
    a {outline: none; text-decoration: none; color: #000000;}
    a:hover {text-decoration: underline; color: #000000;}

    .page {
        margin: 0 auto;
    }

    img{
        border: 0px;
    }

    .panel-default>.panel-heading {
        color: #333;
        background-color: #fff;
        border-color: #e4e5e7;
        padding: 0;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    .panel-default>.panel-heading a {
        display: block;
        padding: 10px 15px;
    }

    .panel-default>.panel-heading a:after {
        content: "";
        position: relative;
        top: 1px;
        display: inline-block;
        font-family: 'Glyphicons Halflings';
        font-style: normal;
        font-weight: 300;
        line-height: 1;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
        float: right;
        transition: transform .25s linear;
        -webkit-transition: -webkit-transform .25s linear;
    }

    .panel-default>.panel-heading a[aria-expanded="true"] {
        background-color: #eee;
    }



    .accordion-option {
        width: 100%;
        float: left;
        clear: both;
        margin: 15px 0;
    }

    .accordion-option .title {
        font-size: 20px;
        font-weight: bold;
        float: left;
        padding: 0;
        margin: 0;
    }

    .accordion-option .toggle-accordion {
        float: right;
        font-size: 16px;
        color: #6a6c6f;
    }


</style>
    <div style="height: 20px;  background-color: white !important;">
        <p style="margin-bottom: 1px; margin-left: 10%; color: black !important; float:left !important">Copyright 2020 | <b>ABC-LEAGUE</b></p>
    </div>
</footer>

<script src="{{asset('js/main.js')}}"></script>

<script src="{{asset('js/flickity.pkgd.min.js')}}"></script>
<script>
    $(document).ready(function(){
        setTimeout(function () {
            $("#cookieConsent").fadeIn(200);
        }, 4000);
        $("#closeCookieConsent, .cookieConsentOK").click(function() {
            $("#cookieConsent").fadeOut(200);
        });
    });
</script>

</html>




<!-------------------------------------------------------------->
<!-------------------------------------------------------------->
<!-------------------------------------------------------------->
<!-------------------------------------------------------------->
<!-------------------------------------------------------------->
<!-------------------------------------------------------------->
<!-------------------------------------------------------------->
<!-------------------------------------------------------------->
<!-------------------------------------------------------------->
<!-------------------------------------------------------------->
<!-------------------------------------------------------------->
@elsedesktop
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        .ukryte{
            display: none;
        ]
    </style>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>ABC League</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="{{asset('vendor/fontawesome-free/css/all.css')}}">
    <script src="{{ asset('js/app.js') }}" defer></script>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    {{--    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>--}}
{{--    <script src="//code.tidio.co/xzdayithnosl44r319fpmvvnxdps1wco.js" async></script>--}}

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/flickity.css')}}" media="screen">

    <nav class="navbar navbar-expand-md navbar-light bg-blue shadow-sm">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
            </a>
            <img class="rounded logo" style="width: 50% !important" src="{{asset('images/white-logo.png')}}"/>

            <button class="navbar-toggler pull-left" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    <li class="nav-item ">
                        <a class="nav-link" href="/accounts">Accounts</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/tos"><b>TOS</b></a>
                    </li>

                    <li class="nav-item">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="{{$start}}">
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <form method="POST" action="{{route('currency')}}">@csrf @method('POST')<input type="hidden" name="currency" value="USD"><input type="hidden" name="name" value="images/dolar.png">
                                <button type="submit" class="dropdown-item" style="background-color: white;" href="#"><img src="{{asset('images/dolar.png')}}" />Dollar</button></form>
                            <form method="POST" action="{{route('currency')}}">@csrf @method('POST')<input style="cyk" type="hidden" name="currency" value="EUR"><input type="hidden"  name="name" value="images/euro.png">
                                <button type="submit" class="dropdown-item" style="background-color: white;" href="#"><img src="{{asset('images/euro.png')}}" />Euro</button></form>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</head>
<body>
<div class="main">
    @yield('content')
</div>
</div>
</body>

<footer>
    <div class="row">
        <div class="col-md">
            <div class="text-center mt-3" style="color: black !important;"> <b>FAQ</b> </div>
            {{--            <ul class="faq-list">--}}
            {{--                <li id="a">1. Can I change the account details?.</li>--}}

            {{--                <div id="a1" class="ukryte"><p style="color: black !important;"><b>-Yes! All of our LoL accounts come with login details so you can log in to your account and change anything you like.</b></p></p>--}}
            {{--            </ul> <ul class="faq-list">--}}
            {{--                <li id="b">2. I have not received an account after purchase.</li>--}}

            {{--                <div id="b1" class="ukryte"><p style="color: black !important;"><b>-Make sure that you check your spam mail folder. If it’s still not there please contact us.</b></p></p>--}}
            {{--            </ul><ul class="faq-list">--}}
            {{--                <li id="c">3. What name will my account have?</li>--}}

            {{--                <div id="c1" class="ukryte"><p style="color: black !important;"><b>-Make sure that you check your spam mail folder. If it’s still not there please contact us.</b></p></p>--}}
            {{--            </ul><ul class="faq-list">--}}
            {{--                <li id="d">4. My account got banned.</li>--}}

            {{--                <div id="d1" class="ukryte"><p style="color: black !important;"><b>-If your account gets banned within 30 days (90 days if you purchased extended warranty) of purchase message us and we will send you a new one. We do not refund money for banned accounts!</b></p></p>--}}
            {{--            </ul>--}}
            {{--            <ul class="faq-list">--}}
            {{--                <li id="e">5. How long does it take to deliver an account?</li>--}}
            {{--                <div id="e1" class="ukryte"><p style="color: black !important;"><b>-Account will be delivered to you instantly after purchase.</b></p></p>--}}

            {{--            </ul>--}}

            <style>
                .btn-link{
                    color: black;

                }
                .accordion > .card .card-header {
                    margin-bottom: -17px !important;
                }
            </style>
            <div class="clearfix"></div>

            <div class="accordion" id="accordionExample" style="">
                <div class="card" style="background-color: #FFFFFF !important; border: none">
                    <div class="card-header" style="background-color: #FFFFFF !important;" id="headingOne">
                        <h2 class="mb-0">
                            <button class="btn btn-link" style="background-color: #FFFFFF !important;cursor: url(images/cursor_2.png), auto !important;" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                <b>1. Can I change the account details?</b>
                            </button>
                        </h2>
                    </div>

                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                        <div class="card-body">
                            Yes! All of our LoL accounts come with login details so you can log in to your account and change anything you like.
                        </div>
                    </div>
                </div>
                <div class="card" style="background-color: #FFFFFF !important; border: none">
                    <div class="card-header" style="background-color: #FFFFFF !important;" id="headingTwo">
                        <h2 class="mb-0">
                            <button class="btn btn-link collapsed" style="background-color: #FFFFFF !important;cursor: url(images/cursor_2.png), auto !important;" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <b>2. I have not received an account</b>
                            </button>
                        </h2>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                        <div class="card-body">
                            Make sure that you check your spam mail folder. If it’s still not there please contact us.                        </div>
                    </div>
                </div>
                <div class="card" style="background-color: #FFFFFF !important; border: none">
                    <div class="card-header" style="background-color: #FFFFFF !important;" id="headingThree">
                        <h2 class="mb-0">
                            <button class="btn btn-link collapsed" style="background-color: #FFFFFF !important;cursor: url(images/cursor_2.png), auto !important;" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <b>3. What name will my account have?</b>
                            </button>
                        </h2>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                        <div class="card-body">
                            The name of the account is randomized, although they sound legit and catchy. If you are not satisfied with your name you can always buy a namechange for 13900 BE in the shop.                            </div>
                    </div>
                </div>
                <div class="card " style="background-color: #FFFFFF !important; border: none">
                    <div class="card-header" style="background-color: #FFFFFF !important;" id="headingFour">
                        <h2 class="mb-0">
                            <button class="btn btn-link collapsed" style="background-color: #FFFFFF !important;cursor: url(images/cursor_2.png), auto !important;" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <b>4. My account got banned</b>
                            </button>
                        </h2>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                        <div class="card-body">
                            If your account gets banned within 30 days (90 days if you purchased extended warranty) of purchase message us and we will send you a new one. We do not refund money for banned accounts!                          </div>
                    </div>
                </div>
                <div class="card" style="background-color: #FFFFFF !important; border: none; ">
                    <div class="card-header" style="background-color: #FFFFFF !important;" id="headingFive">
                        <h2 class="mb-0">
                            <button class="btn btn-link collapsed" style="background-color: #FFFFFF !important;cursor: url({{asset('images/cursor_2.png')}}), auto !important;" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                <b>5. How long does it take to deliver an account?</b>
                            </button>
                        </h2>
                    </div>
                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
                        <div class="card-body">
                            Account will be delivered to you instantly after purchase.                         </div>
                    </div>
                </div>
            </div>
            </br>
            <script>
                var $myGroup = $('#accordion');
                $myGroup.on('show.bs.collapse','.collapse', function() {
                    $myGroup.find('.collapse.show').collapse('hide');
                });
            </script>

            <div class="ukryte"><p style="color: black !important;"><b></b></p></div>
            </ul>
            </br>
            </ul>
        </div>
        <div class="col-sm">
            <p class="text-center mt-3" style="color: black !important;"> <b>Contact with</b> </p>
            <ul class="faq-list">

                <li style="margin-bottom: 2%;cursor: url(images/cursor_2.png), auto !important;"><img style="width:14%" src="{{asset('images/email.png')}}"/> -</li>
                <li style="cursor: url(images/cursor_2.png), auto !important;"><img style="width:14%" src="{{asset('images/discord2.png')}}"/> - </li>
                <li style="cursor: url(images/cursor_2.png), auto !important;"><img style="width:15%" src="{{asset('images/skype.png')}}"/> -</li>

            </ul>
        </div>
        <div class="col-sm"  style="margin-top: -10%; margin-left: 10%;">
            <img class=" mb-5" style=" width: 90%" src="{{asset('images/footer.png')}}"/></br>
            <div style="margin-top: -43%; margin-left: 5%; width:120%">

                <img class=" " style="width: 32%" src="{{asset('images/Payments/Psc.png')}}"/>
                <img class="" style="width: 32%" src="{{asset('images/Payments/Paypal.png')}}"/></br>
                <img class=" " style="width: 70%" src="{{asset('images/Payments/all2.png')}}"/></br>

            </div>
        </div>
    </div>
    </div>
    <hr style="height: 2px; color: black; background-color: black; margin-bottom: 0px !important; margin-top: 0px !important;">

    <script>
        $(document).ready(function() {

            $(".toggle-accordion").on("click", function() {
                var accordionId = $(this).attr("accordion-id"),
                    numPanelOpen = $(accordionId + ' .collapse.in').length;

                $(this).toggleClass("active");

                if (numPanelOpen == 0) {
                    openAllPanels(accordionId);
                } else {
                    closeAllPanels(accordionId);
                }
            })

            openAllPanels = function(aId) {
                console.log("setAllPanelOpen");
                $(aId + ' .panel-collapse:not(".in")').collapse('show');
            }
            closeAllPanels = function(aId) {
                console.log("setAllPanelclose");
                $(aId + ' .panel-collapse.in').collapse('hide');
            }

        });
    </script>
    <style>
        a {outline: none; text-decoration: none; color: #000000;}
        a:hover {text-decoration: underline; color: #000000;}

        .page {
            margin: 0 auto;
        }

        img{
            border: 0px;
        }

        .panel-default>.panel-heading {
            color: #333;
            background-color: #fff;
            border-color: #e4e5e7;
            padding: 0;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        .panel-default>.panel-heading a {
            display: block;
            padding: 10px 15px;
        }

        .panel-default>.panel-heading a:after {
            content: "";
            position: relative;
            top: 1px;
            display: inline-block;
            font-family: 'Glyphicons Halflings';
            font-style: normal;
            font-weight: 300;
            line-height: 1;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            float: right;
            transition: transform .25s linear;
            -webkit-transition: -webkit-transform .25s linear;
        }

        .panel-default>.panel-heading a[aria-expanded="true"] {
            background-color: #eee;
        }



        .accordion-option {
            width: 100%;
            float: left;
            clear: both;
            margin: 15px 0;
        }

        .accordion-option .title {
            font-size: 20px;
            font-weight: bold;
            float: left;
            padding: 0;
            margin: 0;
        }

        .accordion-option .toggle-accordion {
            float: right;
            font-size: 16px;
            color: #6a6c6f;
        }


    </style>
    <div style="height: 20px;  background-color: white !important;">
        <p style="margin-bottom: 1px; margin-left: 10%; color: black !important; float:left !important">Copyright 2020 | <b>ABC-LEAGUE</b></p>
    </div>
</footer>

<script src="{{asset('js/main.js')}}"></script>

<script src="{{asset('js/flickity.pkgd.min.js')}}"></script>
<script>
    $(document).ready(function(){
        setTimeout(function () {
            $("#cookieConsent").fadeIn(200);
        }, 4000);
        $("#closeCookieConsent, .cookieConsentOK").click(function() {
            $("#cookieConsent").fadeOut(200);
        });
    });
</script>
<script>
    $(document).ready(function(){

        $("#closeCookieConsent2, .cookieConsentOK2").click(function() {
            $("#cookieConsent2").fadeOut(200);
        });
    });
</script>
</html>




@enddesktop
