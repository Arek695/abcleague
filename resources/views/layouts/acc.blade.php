<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        .ukryte{
            display: none;
        ]
    </style>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>ABC League</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <!-- Scripts -->
    <!-- JQuery -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.12.0/js/mdb.min.js"></script>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <!-- Google Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.12.0/css/mdb.min.css" rel="stylesheet">

<!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->




    <nav class="navbar navbar-expand-md navbar-light bg-blue shadow-sm">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                <img class="rounded logo" src="{{asset('images/white-logo.png')}}"/>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    <li class="nav-item ">
                        <a class="nav-link" href="/accounts">Accounts</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/tos"><b>TOS</b></a>
                    </li>

                    <li class="nav-item">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="{{$start}}">
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <form method="POST" action="{{route('currency')}}">@csrf @method('POST')<input type="hidden" name="currency" value="USD"><input type="hidden" name="name" value="images/dolar.png">
                                <button type="submit" class="dropdown-item" style="background-color: grey;" href="#"><img src="{{asset('images/dolar.png')}}" />Dollar</button></form>
                            <form method="POST" action="{{route('currency')}}">@csrf @method('POST')<input type="hidden" name="currency" value="EURO"><input type="hidden"  name="name" value="images/euro.png">
                                <button type="submit" class="dropdown-item" style="background-color: grey;" href="#"><img src="{{asset('images/euro.png')}}" />Euro</button></form>


                        </div>
                    </li>
                </ul>
            </div>
        </div>

    </nav>
</head>
<body>
<div class="main">
@yield('content')
</div>
</body>

<footer>
    <div class="row">
        <div class="col-md">
            <div class="text-center mt-3" style="color: black !important;"> <b>FAQ</b> </div>
            <ul class="faq-list">
                <li id="a">1. Can I change the account details?.</li>

                <div id="a1" class="ukryte"><p style="color: black !important;"><b>-Yes! All of our LoL accounts come with login details so you can log in to your account and change anything you like.</b></p></p>
            </ul> <ul class="faq-list">
                <li id="b">2. I have not received an account after purchase.</li>

                <div id="b1" class="ukryte"><p style="color: black !important;"><b>-Make sure that you check your spam mail folder. If it’s still not there please contact us.</b></p></p>
            </ul><ul class="faq-list">
                <li id="c">3. What name will my account have?</li>

                <div id="c1" class="ukryte"><p style="color: black !important;"><b>-Make sure that you check your spam mail folder. If it’s still not there please contact us.</b></p></p>
            </ul><ul class="faq-list">
                <li id="d">4. My account got banned.</li>

                <div id="d1" class="ukryte"><p style="color: black !important;"><b>-If your account gets banned within 30 days (90 days if you purchased extended warranty) of purchase message us and we will send you a new one. We do not refund money for banned accounts!</b></p></p>
            </ul>
            <ul class="faq-list">
                <li id="e">5. How long does it take to deliver an account?</li>
                <div id="e1" class="ukryte"><p style="color: black !important;"><b>-Account will be delivered to you instantly after purchase.</b></p></p>

            </ul>
            </br>

            <script>
                $(document).ready(function(){

                    $("#a").click(function(){
                        $("#a1").show();
                        $("#b1").hide();
                        $("#c1").hide();
                        $("#d1").hide();
                        $("#e1").hide();
                    });
                    $("#b").click(function(){
                        $("#b1").show();
                        $("#a1").hide();
                        $("#c1").hide();
                        $("#d1").hide();
                        $("#e1").hide();
                    });
                    $("#c").click(function(){
                        $("#c1").show();
                        $("#a1").hide();
                        $("#b1").hide();
                        $("#d1").hide();
                        $("#e1").hide();
                    });
                    $("#d").click(function(){
                        $("#d1").show();
                        $("#a1").hide();
                        $("#b1").hide();
                        $("#c1").hide();
                        $("#e1").hide();
                    });
                    $("#e").click(function(){
                        $("#e1").show();
                        $("#a1").hide();
                        $("#b1").hide();
                        $("#c1").hide();
                        $("#d1").hide();
                    });


                });
            </script>
            <div class="ukryte"><p style="color: black !important;"><b></b></p></div>
            </ul>
            </br>
            </ul>
        </div>
        <div class="col-sm">
            <p class="text-center mt-3" style="color: black !important;"> <b>Contact</b> </p>
            <ul class="faq-list">
                <li>L&P </li>
                <li>NIP 9542809607</li>
                <li>REGON 384825556</li>
                <li>Address: Poland, Katowice, Łabędzia 4</li>
                <li>Email: contact@abc-league.com</li>
                <li>Contact: +48 784 907 667</li>
            </ul>
        </div>
        <div class="col-sm"  style="margin-top: -5%;">
            <img class="w-50 mb-5" src="{{asset('images/footer.png')}}"/></br>
            <div style="margin-top: -25%;">

                <img class="w-50 " src="{{asset('images/Payments/all.png')}}"/></br>
                <img class="w-25 " src="{{asset('images/Payments/Psc.png')}}"/>
                <img class="w-25 " src="{{asset('images/Payments/Paypal.png')}}"/></br>
                <img class="w-25" style="" src="{{asset('images/Payments/dotpay2.png')}}"/>
                <img style="margin-left:10%; width: 15%" src="{{asset('images/Payments/bank.png')}}"/>
            </div>
        </div>
    </div>
    </div>
    <hr style="height: 2px; color: black; background-color: black;">

    <div style="height: 20px;  background-color: white !important;">
        <p style="margin-bottom: 1%; margin-left: 10%; color: black !important">Copyright 2020 | <b>ABC-LEAGUE</b></p>
    </div>
</footer>
<script src="{{asset('js/circle-progress.min.js')}}"></script>
<script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/jquery.slicknav.js')}}"></script>
<script src="{{asset('js/owl.carousel.min.js')}}"></script>
<script src="{{asset('js/circle-progress.min.js')}}"></script>
<script src="{{asset('js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>
<script src="{{asset('js/flickity.pkgd.min.js')}}"></script>
<script>
    $(document).ready(function(){
        setTimeout(function () {
            $("#cookieConsent").fadeIn(200);
        }, 4000);
        $("#closeCookieConsent, .cookieConsentOK").click(function() {
            $("#cookieConsent").fadeOut(200);
        });
    });
</script>
<script>
    $(document).ready(function(){

        $("#closeCookieConsent2, .cookieConsentOK2").click(function() {
            $("#cookieConsent2").fadeOut(200);
        });
    });
</script>
</html>
